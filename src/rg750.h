#ifndef RG750_H
#define RG750_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#define MMIO8(addr)   (*(volatile uint8_t *)(addr))
#define MMIO16(addr)   (*(volatile uint16_t *)(addr))
#define MMIO32(addr)   (*(volatile uint32_t *)(addr))

#define RG750_BASE 	0xE0000000
#define RG750_HSTADRL 	(RG750_BASE+0)
#define RG750_HSTADRH 	(RG750_BASE+2)
#define RG750_HSTDATA	(RG750_BASE+4)
#define RG750_HSTCTL	(RG750_BASE+6)

// HSTCTL bits
#define RG750_CTL_MSGIN  (1 << 0)
#define RG750_CTL_INTIN  (1 << 3)
#define RG750_CTL_MSGOUT (1 << 4)
#define RG750_CTL_INTOUT (1 << 7)
#define RG750_CTL_NMI    (1 << 8)
#define RG750_CTL_NMIM   (1 << 9)
#define RG750_CTL_INCW   (1 << 11)
#define RG750_CTL_INCR   (1 << 12)
#define RG750_CTL_LBL    (1 << 13)
#define RG750_CTL_CF     (1 << 14)
#define RG750_CTL_HLT    (1 << 15)

#define RG750_EODLFLAG          0x03000000
#define RG750_KBDFLAG           0x03000010
#define RG750_MSEFLAG           0x03000020
#define RG750_ERRFLAG           0x03000030
#define RG750_IDLEFLAG          0x03000040
#define RG750_DI_COUNT          0x03000050
#define RG750_INTOUTMASK        0x03000060
#define RG750_HOST_FIELD0       0x03000070
#define RG750_HOST_FIELD1       0x03000080
#define RG750_ENV_PTR           0x030000A0
#define RG750_HINT0_AFG_ENTRY   0x030000C0
#define RG750_HINT1_TMS_ENTRY   0x030000E0
#define RG750_GPTABLE_PTR       0x03000100
#define RG750_DEFAULT_ENV_PTR   0x03000120
#define RG750_DPAGEADDR         0x03000140

#define RG750_INT_EODL          0
#define RG750_INT_KBDCHAR       1
#define RG750_INT_SERIAL        2
#define RG750_INT_ERROR         3
#define RG750_INT_60HZ          4

// The AFGIS display list starts at this address.
#define AFGIS_DLIST_BASE    0x03100000

void rg750_set_hstaddr(uint16_t *address);
uint16_t *rg750_get_hstaddr();

#define rg750_set_hstdata(DATA) ( MMIO16(RG750_HSTDATA) = DATA )
uint16_t rg750_get_hstdata();

uint16_t rg750_get_hstctl();
void rg750_set_hstctl(uint16_t data);

uint32_t rg750_get_long(uint16_t *address);

void rg750_reset();

void rg750_enable_incw();
void rg750_send_hint0();

bool rg750_idle();

void rg750_set_displaylist(uint16_t *dl);

void rg750_acknowledge_interrupt();
uint8_t rg750_get_interrupt_num();

uint8_t rg750_wait_for_interrupt();
void setup_vme_irq();

void rg750_transfer_68k_to_gsp(uint16_t *destination, uint16_t *source, uint32_t words_to_transfer);
#endif
