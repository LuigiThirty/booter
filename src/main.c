#include "main.h"

void setup_vme_irq();

extern AIGIS_DisplayList *active_display_list;

char buffer[100];

void SetupSpriteDisplayList();

int main()
{
  printf("Kernel: MVME167 boot\n");
  printf("*** A I G I S   B O O T ***\n");

  int framecounter = 0;
  Rect r;

  bool frame_end_flag = false;
  bool display_list_end_flag = false;

  W_LoadBundle();
  TEX_LoadAllTexturesFromWad(texture_lookup, MAX_TEXTURES);

  setup_vme_irq();
  AIGIS_Init(); // After an aigis_init, DL2 is the edit target.

  AIGIS_IMMEDIATE_LoadImage((GSPPtr)0x03180000, 64, 128, 8, (uint16_t *)texture_lookup[TEX_GetTextureIDByName("STARTAN1")].data);
  AIGIS_IMMEDIATE_LoadImage((GSPPtr)0x03200000, 228, 672, 8, (uint16_t *)texture_lookup[TEX_GetTextureIDByName("S_TYRIAN")].data);
  AIGIS_IMMEDIATE_LoadPaletteData((GSPPtr)AIGIS_PAL_ONE, W_GetLumpDataPtr(W_GetLump("DOOMPAL")), 0, 255);
  AIGIS_IMMEDIATE_LoadPaletteData((GSPPtr)AIGIS_PAL_TWO, W_GetLumpDataPtr(W_GetLump("P_TYRIAN")), 0, 255);

  Point p1, p2, center;
  center.x = 320; center.y = 240;
  p1.x = 320; p1.y = 240;
  p2.x = 420; p2.y = 0;

  // Prepare the display.
  uint8_t dpage = 0;
  uint8_t wpage = 1;

  int x = 600;

  {
    // TODO: make the first display list we run actually work
    AIGIS_DL_Clear();
    AIGIS_DL_End();
    AIGIS_DL_Upload(active_display_list);
    AIGIS_DL_Run();
    AIGIS_wait_for_dl_end_and_frame_end();
    AIGIS_DL_Swap();
  }

  {
    AIGIS_DL_Clear();
    afgis_clrm();
    UseCustomPalette((GSPPtr)AIGIS_PAL_TWO);
    CopyImageToVRAM((GSPPtr)0x03200000, 0, 0);
    AIGIS_DL_End();
    AIGIS_DL_Upload(active_display_list);
    AIGIS_DL_Run();
    AIGIS_wait_for_dl_end_and_frame_end();
    AIGIS_DL_Swap();
  }

  while(framecounter != 2)
  {
    // Start a new display list.
    AIGIS_DL_Clear();

    //SetDisplayPageNum(dpage);
    //SetWritePageNum(wpage);
    afgis_cal(AIGIS_DL_THREE);

    TransparencyMode(0);
    ForeColor(198);
    TextFont(0);
    SetTextLocation(0, 0);
    sprintf(buffer, "framecounter %d\n", framecounter);
    DrawText(buffer);

    afgis_vwait();

    AIGIS_DL_End();
    AIGIS_DL_Upload(active_display_list);
    AIGIS_DL_Run();

    AIGIS_wait_for_dl_end_and_frame_end();

    dpage = (dpage + 1) % 2;
    wpage = (wpage + 1) % 2;
    AIGIS_DL_Swap();

    framecounter++;
  }

  printf("Done!\n");
  AIGIS_DumpAFGISVars();

  printf("*** A I G I S  E N D ***\n");

  MVME_ReturnToDebugger();
 }

void __attribute__((interrupt_handler)) irq6_isr()
{
  printf("IRQ6\n");
  MMIO32(0x000A0000) = 0x12345678;

  // The RG750 sent us an interrupt. Which kind?
  uint16_t data = rg750_get_hstctl();
  if(data & 0x40)
  {
    // 60Hz interrupt.
    MMIO32(0x000A0004) = 0x60606060;
  }
  else
  {
    // Some other interrupt we don't care about.
  }

  data &= 0xFF7F; // clear D7 of HSTCTL to clear the interrupt.
  rg750_set_hstctl(data);
}

/*
  RC_Initialize();
  G_LoadMap("MAP01");

  while(true)
  {
    // Start a new display list.
    AIGIS_DL_Clear();

    SetDisplayPageNum(dpage);
    SetWritePageNum(wpage);

    RC_FrameLoop();

    ForeColor(15);
    TextFont(0);
    SetTextLocation(0, 0);
    sprintf(buffer, "frame %d\n", framecounter);
    DrawText(buffer);
    SetTextLocation(0, 10);
    sprintf(buffer, "rotation X %08X\n", raycasterState.player_direction.x);
    DrawText(buffer);

    afgis_vwait();

    AIGIS_DL_End();
    AIGIS_DL_Run();

    aigis_wait_for_dl_end_and_frame_end();

    dpage = (dpage + 1) % 2;
    wpage = (wpage + 1) % 2;
    AIGIS_DL_Swap();
  }
*/