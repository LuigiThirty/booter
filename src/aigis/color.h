#pragma once

#include "aigis/aigis.h"

void ForeColor(uint8_t color);
void BackColor(uint8_t color);
void SetPalette(uint8_t id);
void UseCustomPalette(GSPPtr address);

void AIGIS_IMMEDIATE_LoadPaletteData(GSPPtr address, uint8_t *paletteData, uint8_t color_start, uint8_t color_count);