#include "aigis/page.h"

void SetDisplayPageNum(uint16_t pageNum)
{
    afgis_dpg(pageNum);
}

void SetDisplayPageAddress(uint16_t *address)
{
    afgis_dpga((uint32_t)address);
}

void SetWritePageNum(uint16_t pageNum)
{
    afgis_wpg(pageNum);
}

void SetWritePageAddress(uint16_t *address)
{
    afgis_wpga((uint32_t)address);
}