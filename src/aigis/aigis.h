#ifndef AIGIS_H
#define AIGIS_H

#include <stdint.h>
#include <stdio.h>
#include "afgis/afgis.h"
#include "aigis/prims.h"
#include "aigis/pointers.h"
#include "rg750.h"

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))

#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480

extern AIGIS_DisplayList *front_list;
extern AIGIS_DisplayList *back_list;

extern AIGIS_DisplayList local1;
extern AIGIS_DisplayList local2;

typedef struct AIGIS_State {
    uint16_t * DRAM_bottom;
    uint16_t * DRAM_top;

    AIGIS_PenState pen_state;
    AIGIS_LineType line_type;
} AIGIS_State;

extern AIGIS_State aigis_state;
extern AIGIS_DisplayList display_list_a;
extern AIGIS_DisplayList display_list_b;

extern AIGIS_State *aigis_current_state;

extern GSPPtr AIGIS_DRAM_BOTTOM;
extern GSPPtr AIGIS_DRAM_TOP;

void AIGIS_Init();
void AIGIS_wait_for_dl_end_and_frame_end();
void AIGIS_DetectDRAM();
void AIGIS_DumpAFGISVars();

#endif
