#include "aigis/fixedpt.h"

FIXED FX_MUL(FIXED a, FIXED b)
{
    // A * B results in an S31.32 number.
    // We need the middle 32 bits.

    uint64_t intermediate;
    intermediate = (uint64_t)a * (uint64_t)b;
    return (uint32_t)(intermediate >> 16);
}

FIXED FX_DIV(FIXED a, FIXED b)
{
    if(b == 0) return 0;
    return ((int64_t)a * (1 << 16)) / b;
}

bool FX_MUL_TESTS()
{
    FIXED a, b, r;

    a = 0x00020000;
    b = 0x00040000;
    r = FX_MUL(a, b);
    if(r != 0x00080000)
    {
        printf("FIXED test 1 fail\n");
        return false;
    }

    a = 0x00028000;
    b = 0x00048000;
    r = FX_MUL(a, b);
    if(r != 0x000B4000)
    {
        printf("FIXED test 2 fail\n");
        return false;
    }

    a = 0xFFFF8000;
    b = 0x00018000;
    r = FX_MUL(a, b);
    if(r != 0xFFFF4000)
    {
        printf("FIXED test 3 fail\n");
        return false;
    }

    a = 0xFFFF8000;
    b = 0xFFFF8000;
    r = FX_MUL(a, b);
    if(r != 0x00004000)
    {
        printf("FIXED test 4 fail\n");
        return false;
    }
    
    return true;
}