#ifndef XYZ_H
#define XYZ_H

#include <stdint.h>

struct XYZ
{
	int16_t x, y, z;
};

void SetXYZ(struct XYZ *xyz, int16_t x, int16_t y, int16_t z);

#endif