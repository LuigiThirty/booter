#include "afgis/afgis.h"

typedef enum AIGIS_LineType
{
    AIGIS_LINE_SOLID,
    AIGIS_LINE_DASH,
    AIGIS_LINE_DASH_ARB, // arbitrary
    AIGIS_LINE_FATLINE_SOLID,
    AIGIS_LINE_FATLINE_STIPPLE,
    AIGIS_LINE_FATLINE_TILE,
    AIGIS_LINE_PEN_SOLID,
    AIGIS_LINE_PEN_STIPPLE,
    AIGIS_LINE_PEN_TILE,
} AIGIS_LineType;

void LineTo(AIGIS_LineType linetype, int16_t x, int16_t y);
void Line(AIGIS_LineType linetype, int16_t x0, int16_t y0, int16_t x1, int16_t y1);