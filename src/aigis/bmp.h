#pragma once

#include "afgis/afgis.h"
#include "aigis/types.h"

void AIGIS_IMMEDIATE_LoadImage(GSPPtr address, uint16_t width, uint16_t height, uint16_t bpp, uint16_t *imagedata);
void CopyImageToVRAM(GSPPtr source, int16_t x, int16_t y);