#include "aigis/bmp.h"
#include "raycast/w_wadmgr.h"

void AIGIS_IMMEDIATE_LoadImage(GSPPtr address, uint16_t width, uint16_t height, uint16_t bpp, uint16_t *imagedata)
{
    // Upload an image to the GSP.
    // Row pitch must be a multiple of 16.

    afgis_image_data image_info;
    image_info.width = width;
    image_info.height = height;
    image_info.bpp = bpp;
    switch(bpp)
    {
        case 1:
            image_info.pitch = width/8 * 8;
            break;
        case 2:
            image_info.pitch = width/4 * 8;
            break;
        case 4:
            image_info.pitch = width/2 * 8;
            break;
        case 8:
            image_info.pitch = width * 8;
            break;
        default:
            printf("AIGIS_IMMEDIATE_LoadImage: Can't load an image of %d bpp\n", bpp);
            break;
    }

    int32_t transfer_size = (width*height)/2; // TODO, only care about 8bpp for now. transfer width is 16 bits

    GSPPtr old_hstadr = rg750_get_hstaddr();

    rg750_set_hstaddr(address);
    
    // Upload the image info structure, then the pixel block.
    rg750_set_hstdata(image_info.width);
    rg750_set_hstdata(image_info.height);
    rg750_set_hstdata(image_info.bpp);
    rg750_set_hstdata(image_info.pitch);
    rg750_set_hstdata(0);   // two reserved words
    rg750_set_hstdata(0);

    while(transfer_size > 0)
    {
        // TODO: Read rows bottom-up to match the BMP format.
        rg750_set_hstdata(W_SwapEndianness16(*imagedata++));
        transfer_size--;
    }

    rg750_set_hstaddr(old_hstadr);
}

void CopyImageToVRAM(GSPPtr source, int16_t x, int16_t y)
{
    afgis_copyrs((LONG)source, x, y);
}