#include "aigis/rect.h"
#include "aigis.h"

void SetRect(Rect *r, int16_t left, int16_t top, int16_t right, int16_t bottom)
{
    r->x = left;
    r->y = top;
    r->w = right-left;
    r->h = bottom-top;    
}

void FrameRect(Rect *r)
{
    afgis_rect(aigis_current_state->line_type, r->x, r->y, r->x+r->w, r->y+r->h);
}

void PaintRect(Rect *r, uint8_t w_type)
{
    afgis_rects(w_type, r->x, r->y, r->x+r->w, r->y+r->h);
}