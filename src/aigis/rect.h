#pragma once

#include <stdint.h>
#include "aigis/types.h"
#include "afgis/afgis.h"

void SetRect(Rect *r, int16_t left, int16_t top, int16_t right, int16_t bottom);

void FrameRect(Rect *r);
void PaintRect(Rect *r, uint8_t w_type);