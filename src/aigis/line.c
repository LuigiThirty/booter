#include "aigis/line.h"
#include "aigis/dlist.h"

void LineTo(AIGIS_LineType linetype, int16_t x, int16_t y)
{
    afgis_lineto(linetype, x, y);
}

void Line(AIGIS_LineType linetype, int16_t x0, int16_t y0, int16_t x1, int16_t y1)
{
    afgis_line(linetype, x0, y0, x1, y1);
}