#include "dlist.h"

AIGIS_DisplayList *active_display_list;
AIGIS_DisplayList *local_active_display_list;

AIGIS_DisplayList *front_list;
AIGIS_DisplayList *back_list;

void AIGIS_DL_EditDisplayList(AIGIS_DisplayList *display_list)
{
    // This is the target of display list edit functions.
    //printf("Display list edit address: %08X\n", display_list->start);
    active_display_list = display_list;
    rg750_set_hstaddr((uint16_t *)display_list->start);
}

void AIGIS_DL_AddWord(WORD word)
{
    *active_display_list->end = word;
    active_display_list->end++;
}

void AIGIS_DL_Clear()
{
    // Clear the active display list by setting END to START.
    active_display_list->end = active_display_list->start;
}

void AIGIS_DL_End()
{
    // End the current display list.
    afgis_eodl();
}

void AIGIS_DL_Run()
{
    // Run the active display list immediately.
    rg750_send_hint0();    
}
void AIGIS_DL_Swap()
{
    // Swap the front and back lists.
    AIGIS_DisplayList *temp;
    temp = back_list;
    back_list = front_list;
    front_list = temp;

    AIGIS_DL_EditDisplayList(back_list);
    rg750_set_displaylist((uint16_t *)front_list->remote_start);

    rg750_set_hstaddr((uint16_t *)back_list->remote_start);

    //printf("Now editing display list at %08X\n", back_list->start);
}

OPCODE * AIGIS_DL_Allocate()
{
    return (OPCODE *)malloc(sizeof(OPCODE) * 1024);
}

void AIGIS_DL_Upload()
{
    //printf("Aigis: Uploading display list at 68K %08X to GSP %08X\n", active_display_list->start, active_display_list->remote_start);

    /*
    rg750_set_hstaddr(active_display_list->remote_start);
    MMIO32(L2V_DMAC_LBADR) = (uint32_t)active_display_list->start;
    MMIO32(L2V_DMAC_VMEADR) = RG750_HSTDATA;
    MMIO32(L2V_DMAC_BYTES) = (active_display_list->end - active_display_list->start) * 2;
    MMIO32(L2V_DMAC2_REG) = L2V_DMAC2_D16|L2V_DMAC2_TVME|L2V_DMAC2_LINC;
    MMIO32(L2V_DMAC1_REG) = (MMIO32(L2V_DMAC1_REG) & 0xFFFFFF00)|L2V_DMAC1_DEN|L2V_DMAC1_DFAIR|0x0F; // ?

    while(MMIO32(L2V_DMAC_BYTES) != 0) {}
    printf("Aigis: DMA transfer complete\n");
    MMIO32(L2V_DMAC1_REG) = (MMIO32(L2V_DMAC1_REG) & 0xFFFFFF00);
    printf("LBADDR %08X VMEADDR %08X HSTADR %08X\n", MMIO32(L2V_DMAC_LBADR), MMIO32(L2V_DMAC_VMEADR), rg750_get_hstaddr());
    printf("DMA status %02X\n", MMIO32(0xFFF40048) & 0xFF);
    */

    rg750_set_hstaddr(active_display_list->remote_start);
    OPCODE *current = active_display_list->start;
    while(current != active_display_list->end)
    {
        rg750_set_hstdata(*current);
        current++;
    }
}