#include "aigis/color.h"

void ForeColor(uint8_t color)
{
    afgis_colorf(color);
}

void BackColor(uint8_t color)
{
    afgis_colorb(color);
}

void AIGIS_IMMEDIATE_LoadPaletteData(GSPPtr address, uint8_t *paletteData, uint8_t color_start, uint8_t color_count)
{
    GSPPtr old_hstadr = rg750_get_hstaddr();

    printf("Aigis: Loading palette at 68K %08X to GSP %08X\n", paletteData, address);

    rg750_set_hstaddr(address);
    rg750_set_hstdata(0xFFFF);
    rg750_set_hstdata(0xFFFF);
    rg750_set_hstdata(color_start);
    rg750_set_hstdata(color_count);

    // TODO: Detect .COL format palettes which have an 8-byte header.

    int color_idx = 0;
    while(color_count > 0)
    {
        // Palette data is RGB, need to flip it to ABGR.
        uint16_t color_hi;
        uint16_t color_lo;

        uint8_t r = *paletteData++;
        uint8_t g = *paletteData++;
        uint8_t b = *paletteData++;

        color_lo = g;
        color_lo = color_lo << 8;
        color_lo |= r;
        color_hi = b;

        if(color_idx < 8)
        {
            //printf("color %d: %d,%d,%d\n", color_idx++, r, g, b);
        }

        rg750_set_hstdata(color_lo);
        rg750_set_hstdata(color_hi);

        color_count--;
    }

    rg750_set_hstaddr(old_hstadr);
}

void SetPalette(uint8_t id)
{

}

void UseCustomPalette(GSPPtr address)
{
    afgis_setpalette(0, (LONG)address);
}