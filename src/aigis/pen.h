#include "afgis/afgis.h"

typedef struct AIGIS_PenState {
    int16_t x, y;
} AIGIS_PenState;

void MoveTo(int16_t x, int16_t y);
void Move(int16_t x, int16_t y);