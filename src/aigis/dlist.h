#pragma once

#include <stdio.h>
#include <stdlib.h>
#include "afgis/afgis.h"

typedef struct AIGIS_DisplayList
{
    // Display lists are stored in 68K RAM and transferred to the GSP via DMA.
    uint16_t *start;        // Start of this display list.
    uint16_t *end;          // End of this display list.

    OPCODE *remote_start;   // GSP address.
} AIGIS_DisplayList;

extern AIGIS_DisplayList *front_list;
extern AIGIS_DisplayList *back_list;

extern AIGIS_DisplayList dl1;
extern AIGIS_DisplayList dl2;

extern AIGIS_DisplayList local1;
extern AIGIS_DisplayList local2;

void AIGIS_DL_EditDisplayList(AIGIS_DisplayList *display_list);

OPCODE * AIGIS_DL_Allocate();

// All of these apply to the active display list.
void AIGIS_DL_AddWord(WORD word);
void AIGIS_DL_Upload();
void AIGIS_DL_Clear();
void AIGIS_DL_End();
void AIGIS_DL_Run();
void AIGIS_DL_Swap();