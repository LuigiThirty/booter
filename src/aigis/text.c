#include "aigis/text.h"
#include "aigis/dlist.h"

// No quickdraw equivalent just for text, I think.
void SetTextLocation(int16_t w1, int16_t w2)
{
    afgis_ctextlxy(w1, w2);
}

void DrawText(char *str)
{
    afgis_ctexti(str);
}

void TextFont(int16_t fontnum)
{
    afgis_font(fontnum);
}