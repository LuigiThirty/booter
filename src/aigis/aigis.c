#include "aigis.h"

AIGIS_State aigis_state;
AIGIS_State *aigis_current_state;

AIGIS_DisplayList local1;
AIGIS_DisplayList local2;

AIGIS_DisplayList sprite_dl;

void AIGIS_SetupSpriteDisplayList()
{
  // AFGIS subroutine that does sprite stuff.
  // Generate the display list and write it into slot 3.
  AIGIS_DL_EditDisplayList(&sprite_dl);

  // Display list START

  // COPYSS uses posx,posy,endx,endy,destx,desty.
  afgis_ldiv(16, AFGIS_V10);                  // V10  <- 16 bits to add to the pointer...

  afgis_ldivl(AIGIS_SPRITE_BASE, AFGIS_V0);   // V0   <- sprite array base
  afgis_rpt(16);                              // loop 16 times
  afgis_ldpmv(AFGIS_V0, AFGIS_V1);            // V1   <- (V0).W, posx

  afgis_advv(AFGIS_V10, AFGIS_V0);            // V0   <- V0 + 16 bits
  afgis_ldpmv(AFGIS_V0, AFGIS_V2);            // V2   <- (V0).W, posy

  afgis_advv(AFGIS_V10, AFGIS_V0);            // V0   <- V0 + 16 bits
  afgis_ldpmv(AFGIS_V0, AFGIS_V3);            // V3   <- (V0).W, endx

  afgis_advv(AFGIS_V10, AFGIS_V0);            // V0   <- V0 + 16 bits
  afgis_ldpmv(AFGIS_V0, AFGIS_V4);            // V4   <- (V0).W, endy

  afgis_advv(AFGIS_V10, AFGIS_V0);            // V0   <- V0 + 16 bits
  afgis_ldpmv(AFGIS_V0, AFGIS_V5);            // V5   <- (V0).W, destx

  afgis_advv(AFGIS_V10, AFGIS_V0);            // V0   <- V0 + 16 bits
  afgis_ldpmv(AFGIS_V0, AFGIS_V6);            // V6   <- (V0).W, desty
  
  afgis_advv(AFGIS_V10, AFGIS_V0);            // V0   <- V0 + 16 bits

  afgis_copyssv(AFGIS_V1);                    // COPYSS(v1,v2,v3,v4,v5,v6)
  afgis_erpt();                               // end loop
  //afgis_copyss(51, 170, 51+17, 170+24, 600, 300);

  afgis_rtrn();
  // Display list END

  // Upload to slot 3.
  AIGIS_DL_Upload();
}

void AIGIS_Init()
{
    printf("AIGIS_Init: Initializing RG750. Setting up AFGIS defaults.\n");
    rg750_reset();
    for(int i=0; i<100000; i++) {} // TODO: Figure out reset timing.
    rg750_enable_incw(); 

    aigis_state.line_type = AIGIS_LINE_SOLID;
    aigis_state.pen_state.x = 0;
    aigis_state.pen_state.y = 0;
    aigis_current_state = &aigis_state;

    AIGIS_DetectDRAM();
    AIGIS_SPR_Init();

    front_list = &local1;
    back_list  = &local2;

    rg750_set_hstaddr((uint16_t *)RG750_HINT0_AFG_ENTRY);
    rg750_set_hstdata(0x0000); 
    rg750_set_hstdata(0x0308);

    rg750_set_hstaddr((uint16_t *)0x03000060);
    rg750_set_hstdata(0x0011); // enable 60Hz interrupt and display list interrupt

    sprite_dl.start = AIGIS_DL_Allocate();
    sprite_dl.end = sprite_dl.start;
    sprite_dl.remote_start = (GSPPtr)AIGIS_DL_THREE;
    AIGIS_SetupSpriteDisplayList();

    local1.start = AIGIS_DL_Allocate();
    local1.end = local1.start;
    local1.remote_start = (GSPPtr)AIGIS_DL_ONE;
    local2.start = AIGIS_DL_Allocate();
    local2.end = local2.start;
    local2.remote_start = (GSPPtr)AIGIS_DL_TWO;

    AIGIS_DL_EditDisplayList(&local2);
}

void AIGIS_wait_for_dl_end_and_frame_end()
{
    bool display_list_end_flag = false;
    bool frame_end_flag = false;
    while((display_list_end_flag == false) || (frame_end_flag == false))
    {
        uint8_t irq_type = rg750_wait_for_interrupt();
        if(irq_type == RG750_INT_EODL) display_list_end_flag = true;
        else if(irq_type == RG750_INT_60HZ) frame_end_flag = true;

        rg750_acknowledge_interrupt();
    }
}

void AIGIS_DetectDRAM()
{
  rg750_set_hstaddr((uint16_t *)0x03800000);
  rg750_set_hstdata(0x1234);
  rg750_set_hstaddr((uint16_t *)0x03800000);
  if(rg750_get_hstdata() != 0x1234)
  {
    aigis_state.DRAM_bottom = (GSPPtr)0x03000000;
    aigis_state.DRAM_top = (GSPPtr)0x037FFFF0;
    printf("RG750: 1M DRAM detected\n");
  }
  else 
  {
    aigis_state.DRAM_bottom = (GSPPtr)0x03000000;
    aigis_state.DRAM_top = (GSPPtr)0x04FFFFF0;
    printf("RG750: 4M DRAM detected\n");
  }
}

void AIGIS_DumpAFGISVars()
{
  // Default environment is at $03004D00.
  // AFGIS variables are at $03007140.

  // TODO: This is only valid for the default environment.
  uint32_t afgis_vars[64];

  uint32_t base = 0x03007140;
  for(int i=0;i<64;i++)
  {
    rg750_set_hstaddr(base);
    afgis_vars[i] = rg750_get_hstdata();
    base += 0x10;
    rg750_set_hstaddr(base);
    afgis_vars[i] |= ((uint32_t)rg750_get_hstdata()) << 16;
    base += 0x10;
  }

  for(int var=0;var<64;var += 4)
  {
    printf("V%02d: %08X V%02d: %08X V%02d: %08X V%02d: %08X\n", 
      var, afgis_vars[var], 
      var+1, afgis_vars[var+1],
      var+2, afgis_vars[var+2],
      var+3, afgis_vars[var+3]
    );
  }
}