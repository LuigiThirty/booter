#include "aigis/sprite.h"

AIGIS_Sprite Sprites[16];

void AIGIS_SPR_Upload(GSPPtr destination, AIGIS_Sprite *source)
{
    GSPPtr old = rg750_get_hstaddr();

    uint16_t words_to_transfer = ((uint32_t)&source[16] - (uint32_t)source) / 2;
    printf("Transfer size is %d words (%08X - %08X))\n", words_to_transfer, source, source+AIGIS_SPRITE_COUNT);
    rg750_transfer_68k_to_gsp(destination, (uint16_t *)source, words_to_transfer);

    rg750_set_hstaddr(old);
}

void AIGIS_SPR_Init()
{
    printf("Aigis: Initializing sprite data at 68K %08X and uploading to GSP %08X\n", Sprites, AIGIS_SPRITE_BASE);

    for(int i=0;i<AIGIS_SPRITE_COUNT;i++)
    {
        Sprites[i].source_x = 51;
        Sprites[i].source_y = 170;
        Sprites[i].size_x = 51+17;
        Sprites[i].size_y = 170+24;
        Sprites[i].pos_x = 300 + (i*20);
        Sprites[i].pos_y = 200;
    }

    AIGIS_SPR_Upload((GSPPtr)AIGIS_SPRITE_BASE, Sprites);
}