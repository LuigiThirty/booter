#ifndef XY_H
#define XY_H

#include <stdint.h>

enum FILL_SHAPE {
	FILL_YES,
	FILL_NO
};

struct XY
{
	int16_t x, y;
};

void SetXY(struct XY *xy, int16_t x, int16_t y);

#endif