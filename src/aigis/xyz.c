#include "aigis/xyz.h"

void SetXYZ(struct XYZ *xyz, int16_t x, int16_t y, int16_t z)
{
    xyz->x = x;
    xyz->y = y;
    xyz->z = z;
}