#pragma once

#include <stdint.h>
#include "fixedpt.h"

typedef uint16_t* GSPPtr;

#pragma pack(1)
struct BMPHeader {
    char header_id[2];      // 0
    uint32_t byte_size;     // 2
    uint16_t reserved1;     // 6
    uint16_t reserved2;     // 8
    uint8_t *image_data;    // 10 - offset from start of WAD

    uint32_t bitmapcoreheader_size; // 14
    uint32_t image_width;   // 18
    uint32_t image_height;  // 22
    uint16_t planes;        /* 1 */
    uint16_t bpp;           /* 8 */
};
#pragma pack()

typedef struct Point
{
    int16_t x, y;
} Point;

typedef struct Rect
{
    int16_t x, y, w, h;
} Rect;