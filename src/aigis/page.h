#pragma once

#include "afgis/afgis.h"

void SetDisplayPageNum(uint16_t pageNum);
void SetWritePageNum(uint16_t pageNum);
void SetDisplayPageAddress(uint16_t *address);
void SetWritePageAddress(uint16_t *address);