#pragma once

#include <stdint.h>
#include "aigis/types.h"
#include "afgis/afgis.h"
#include "aigis/pointers.h"

#pragma pack(0)
// Update the sprite data definition in pointers.h if this is changed!
typedef struct AIGIS_Sprite {
    // This structure lives in GSP DRAM.
    // This structure matches up with the COPYSS opcode.
    uint16_t source_x;  // 0
    uint16_t source_y;  // 2
    uint16_t size_x;    // 4
    uint16_t size_y;    // 6
    int16_t pos_x;      // 8
    int16_t pos_y;      // 10
                        // 12 bytes
} AIGIS_Sprite;
#pragma pack()

extern AIGIS_Sprite Sprites[AIGIS_SPRITE_COUNT];

void AIGIS_SPR_Init();