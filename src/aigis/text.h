#include "afgis/afgis.h"

void TextFont(int16_t);
void SetTextLocation(int16_t w1, int16_t w2);
void DrawText(char *str);