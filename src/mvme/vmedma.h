#pragma once

#include <stdio.h>
#include <stdlib.h>
#include "rg750.h"

#define L2V_CTRL_REG    0xFFF40030
#define L2V_CTRL_LVREQL (1 << 8)
#define L2V_CTRL_LVRWD  (1 << 10)
#define L2V_CTRL_LVFAIR (1 << 11)
#define L2V_CTRL_DWB    (1 << 13)
#define L2V_CTRL_DHB    (1 << 14)
#define L2V_CTRL_ROBN   (1 << 15)

#define L2V_DMAC1_REG   0xFFF40030
#define L2V_DMAC1_DRELQ (1 << 0)
#define L2V_DMAC1_DRELM (1 << 2)
#define L2V_DMAC1_DFAIR (1 << 4)
#define L2V_DMAC1_DTBL  (1 << 5)
#define L2V_DMAC1_DEN   (1 << 6)
#define L2V_DMAC1_DHALT (1 << 7)

#define L2V_DMAC2_REG   0xFFF40034
#define L2V_DMAC2_VMEAM (1 << 0)
#define L2V_DMAC2_BLK   (1 << 6)
#define L2V_DMAC2_D16   (1 << 8)
#define L2V_DMAC2_TVME  (1 << 9)
#define L2V_DMAC2_LINC  (1 << 10)
#define L2V_DMAC2_VINC  (1 << 11)
#define L2V_DMAC2_SNP   (1 << 13)
#define L2V_DMAC2_INTE  (1 << 15)

#define L2V_DMAC_LBADR  0xFFF40038
#define L2V_DMAC_VMEADR 0xFFF4003C
#define L2V_DMAC_BYTES  0xFFF40040
