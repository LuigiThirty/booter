    .global MVME_ReturnToDebugger

    .text
MVME_ReturnToDebugger:
    trap    #15
    dc.w    0x0063
    rts
