#pragma once

#include <math.h>
#include <stdint.h>

#include "aigis/aigis.h"

#define DEGtoRAD(angleInDegrees) ((angleInDegrees) * M_PI / 180.0)
#define RADtoDEG(angleInRadians) ((angleInRadians) * 180.0 / M_PI)

void Point_RotateDegrees(Point *p, Point *center, int degrees);