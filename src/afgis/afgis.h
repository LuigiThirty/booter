#ifndef AFGIS_H
#define AFGIS_H

#include <stdint.h>
#include <string.h>
#include "afgis/afgisops.h"
#include "rg750.h"
#include "mvme/vmedma.h"

typedef uint16_t OPCODE;
typedef int16_t  WORD;
typedef int32_t  LONG;

// AFGIS opcode wrappers.
uint8_t afgis_op_implied(OPCODE);
uint8_t afgis_op_long(OPCODE, LONG);
uint8_t afgis_op_word(OPCODE, WORD);
uint8_t afgis_op_word_word(OPCODE, WORD, WORD);
uint8_t afgis_op_word_long(OPCODE, WORD, LONG);
uint8_t afgis_op_long_word(OPCODE, LONG, WORD);
uint8_t afgis_op_word_word_word(OPCODE, WORD, WORD, WORD);
uint8_t afgis_op_word_word_long(OPCODE, WORD, WORD, LONG);
uint8_t afgis_op_long_word_word(OPCODE, LONG, WORD, WORD);
uint8_t afgis_op_long_long_long(OPCODE, LONG, LONG, LONG);
uint8_t afgis_op_word_word_word_word(OPCODE, WORD, WORD, WORD, WORD);
uint8_t afgis_op_word_word_word_word_long(OPCODE, WORD, WORD, WORD, WORD, LONG);
uint8_t afgis_op_word_word_word_word_word(OPCODE, WORD, WORD, WORD, WORD, WORD);
uint8_t afgis_op_word_word_word_word_word_word(OPCODE, WORD, WORD, WORD, WORD, WORD, WORD);
uint8_t afgis_op_word_word_word_word_word_word_word(OPCODE, WORD, WORD, WORD, WORD, WORD, WORD, WORD);

/* AFGIS functions insert an entry into the display list. */
#define afgis_colorf(VALUE)                 ( afgis_op_long(AFGIS_OP_COLORF, VALUE) )
#define afgis_colorb(VALUE)                 ( afgis_op_long(AFGIS_OP_COLORB, VALUE) )
#define afgis_ctextlxy(W1,W2)               ( afgis_op_word_word(AFGIS_OP_CTEXTLXY, W1, W2) )
#define afgis_eodl()                        ( afgis_op_implied(AFGIS_OP_EODL) )
#define afgis_font(VALUE)                   ( afgis_op_long(AFGIS_OP_FONT, VALUE) )
#define afgis_led(VALUE)                    ( afgis_op_word(AFGIS_OP_LED, VALUE) )
#define afgis_line(W1,W2,W3,W4,W5)          ( afgis_op_word_word_word_word_word(AFGIS_OP_LINE, W1, W2, W3, W4, W5) )
#define afgis_lineto(W1,W2,W3)              ( afgis_op_word_word_word(AFGIS_OP_LINETO, W1, W2, W3) )
#define afgis_moveto(W1,W2)                 ( afgis_op_word_word(AFGIS_OP_MOVETO, W1, W2) )
#define afgis_movetor(W1,W2)                ( afgis_op_word_word(AFGIS_OP_MOVETOR, W1, W2) )
#define afgis_rect(W1,W2,W3,W4,W5)          ( afgis_op_word_word_word_word_word(AFGIS_OP_RECT, W1, W2, W3, W4, W5) )
#define afgis_rects(W1,W2,W3,W4,W5)         ( afgis_op_word_word_word_word_word(AFGIS_OP_RECTS, W1, W2, W3, W4, W5) )
#define afgis_rrect(W1,W2,W3,W4,W5,W6,W7)   ( afgis_op_word_word_word_word_word(AFGIS_OP_RRECT, W1, W2, W3, W4, W5, W6, W7) )
#define afgis_rrects(W1,W2,W3,W4,W5,W6,W7)  ( afgis_op_word_word_word_word_word(AFGIS_OP_RRECTS, W1, W2, W3, W4, W5, W6, W7) )

// AFGIS Variable Operations
#define afgis_adiv(L1,W1)                   ( afgis_op_long_word(AFGIS_OP_ADIV,L1,W1) )
#define afgis_advv(W1,W2)                   ( afgis_op_word_word(AFGIS_OP_ADVV,W1,W2) )
#define afgis_andiv(L1,W1)                  ( afgis_op_long_word(AFGIS_OP_ANDIV,L1,W1) )
#define afgis_andvv(W1,W2)                  ( afgis_op_word_word(AFGIS_OP_ANDVV,W1,W2) )
#define afgis_cpiv(L1,W1)                   ( afgis_op_long_word(AFGIS_OP_CPRIV,L1,W1) )
#define afgis_cpvv(W1,W2)                   ( afgis_op_word_word(AFGIS_OP_CPVV,W1,W2) )
#define afgis_dcrv(W1)                      ( afgis_op_word(AFGIS_OP_DCRV,W1) )
#define afgis_divv(W1,W2)                   ( afgis_op_word_word(AFGIS_OP_DIVV,W1,W2) )
#define afgis_incv(W1)                      ( afgis_op_word(AFGIS_OP_INCV,W1) )
#define afgis_ldiv(W1,W2)                   ( afgis_op_word_word(AFGIS_OP_LDIV,W1,W2) )
#define afgis_ldivl(L1,W1)                  ( afgis_op_long_word(AFGIS_OP_LDIVL,L1,W1) )
#define afgis_ldmv(L1,W1)                   ( afgis_op_long_word(AFGIS_OP_LDMV,L1,W1) )
#define afgis_ldmvl(L1,W1)                  ( afgis_op_long_word(AFGIS_OP_LDMVL,L1,W1) )
#define afgis_ldpcv(W1)                     ( afgis_op_word(AFGIS_OP_LDPCV,W1) )
#define afgis_ldpmv(W1,W2)                  ( afgis_op_word_word(AFGIS_OP_LDPMV,W1,W2) )
#define afgis_ldpmvl(W1,W2)                 ( afgis_op_word_word(AFGIS_OP_LDPMVL,W1,W2) )
#define afgis_ldspv(W1)                     ( afgis_op_word(AFGIS_OP_LDSPV,W1) )
#define afgis_ldvm(W1,L1)                   ( afgis_op_word_long(AFGIS_OP_LDVM,W1,L1) )
#define afgis_ldvml(W1,L1)                  ( afgis_op_word_long(AFGIS_OP_LDVML,W1,L1) )
#define afgis_ldvpm(W1,W2)                  ( afgis_op_word_word(AFGIS_OP_LDVPM,W1,W2) )
#define afgis_ldvpml(W1,W2)                 ( afgis_op_word_word(AFGIS_OP_LDVPML,W1,W2) )
#define afgis_ldvv(W1,W2)                   ( afgis_op_word_word(AFGIS_OP_LDVV,W1,W2) )
#define afgis_mltv(W1,W2)                   ( afgis_op_word_word(AFGIS_OP_MLTV,W1,W2) )
#define afgis_modv(W1,W2)                   ( afgis_op_word_word(AFGIS_OP_MODV,W1,W2) )
#define afgis_oriv(L1,W1)                   ( afgis_op_long_word(AFGIS_OP_ORIV,L1,W1) )
#define afgis_orvv(W1,W2)                   ( afgis_op_word_word(AFGIS_OP_ORVV,W1,W2) )
#define afgis_popv(W1)                      ( afgis_op_word(AFGIS_OP_POPV,W1) )
#define afgis_popvars(W1,W2)                ( afgis_op_word_word(AFGIS_OP_POPVARS,W1,W2) )
#define afgis_pushv(W1)                     ( afgis_op_word(AFGIS_OP_PUSHV,W1) )
#define afgis_pushvars(W1,W2)               ( afgis_op_word_word(AFGIS_OP_PUSHVARS,W1,W2) )
#define afgis_sbiv(L1,W1)                   ( afgis_op_long_word(AFGIS_OP_SBIV,L1,W1) )
#define afgis_sbvv(W1,W2)                   ( afgis_op_word_word(AFGIS_OP_SBVV,W1,W2) )
#define afgis_sllv(W1,W2)                   ( afgis_op_word_word(AFGIS_OP_SLLV,W1,W2) )
#define afgis_srlv(W1,W2)                   ( afgis_op_word_word(AFGIS_OP_SRLV,W1,W2) )
#define afgis_xchgpc(W1)                    ( afgis_op_word(AFGIS_OP_XCHGPC,W1) )
#define afgis_xchgsp(W1)                    ( afgis_op_word(AFGIS_OP_XCHGSP,W1) )
#define afgis_xoriv(L1,W1)                  ( afgis_op_long_word(AFGIS_OP_XORIV,L1,W1) )
#define afgis_xorvv(W1,W2)                  ( afgis_op_word_word(AFGIS_OP_XORVV,W1,W2) )

// Area Fill
#define afgis_tilex(W1,W2,W3,W4,L1)         ( afgis_op_word_word_word_word_long(AFGIS_OP_TILEX,W1,W2,W3,W4,L1) )

// Clipping
#define afgis_clipwin(W1,W2,W3,W4)          ( afgis_op_word_word_word_word(AFGIS_OP_CLIPWIN,W1,W2,W3,W4) )

// Color
#define afgis_setrgb(W1,W2,L1)              ( afgis_op_word_word_long(AFGIS_OP_SETRGB,W1,W2,L1) )
#define afgis_setpalette(W1,L1)             ( afgis_op_word_long(AFGIS_OP_SETPALETTE,W1,L1) )

// Image Operations
#define afgis_copyrs(L1,W1,W2)              ( afgis_op_long_word_word(AFGIS_OP_COPYRS,L1,W1,W2) )
#define afgis_copysr(W1,W2,W3,W4,L1)        ( afgis_op_word_word_word_word_long(AFGIS_OP_COPYSR,W1,W2,W3,W4,L1) )
#define afgis_copyss(W1,W2,W3,W4,W5,W6)     ( afgis_op_word_word_word_word_word_word(AFGIS_OP_COPYSS,W1,W2,W3,W4,W5,W6) )
#define afgis_copyssv(W1)                   ( afgis_op_word(AFGIS_OP_COPYSSV,W1) )

// Pixel Processign
#define afgis_bool(W1)                      ( afgis_op_word(AFGIS_OP_BOOL,W1) )
#define afgis_pmask(L1)                     ( afgis_op_long(AFGIS_OP_PMASK,W1) )
#define afgis_trans(W1)                     ( afgis_op_word(AFGIS_OP_TRANS,W1) )

// Program Flow
#define afgis_cal(L1)                       ( afgis_op_long(AFGIS_OP_CAL,L1) )
#define afgis_calv(W1)                      ( afgis_op_word(AFGIS_OP_CALV,W1 ) )
#define afgis_erpt()                        ( afgis_op_implied(AFGIS_OP_ERPT) )
#define afgis_jumpa(W1,L1)                  ( afgis_op_word_long(AFGIS_OP_JUMPA,W1,L1) )
#define afgis_jumpav(W1)                    ( afgis_op_word(AFGIS_OP_JUMPRV,W1 ) )
#define afgis_jumpr(W1,L1)                  ( afgis_op_word_long(AFGIS_OP_JUMPR,W1,L1) )
#define afgis_jumprv(W1)                    ( afgis_op_word(AFGIS_OP_JUMPRV,W1 ) )
#define afgis_rpt(W1)                       ( afgis_op_word(AFGIS_OP_RPT,W1) )
#define afgis_rtrn()                        ( afgis_op_implied(AFGIS_OP_RTRN) )
#define afgis_vwait()                       ( afgis_op_implied(AFGIS_OP_VWAIT) )

// Utilities
#define afgis_copyrr(L1,L2,L3)              ( afgis_op_long_long_long(AFGIS_OP_COPYRR,L1,L2,L3) )

// Video Page Operations
#define afgis_clrm()                        ( afgis_op_implied(AFGIS_OP_CLRM) )
#define afgis_dpg(W1)                       ( afgis_op_word(AFGIS_OP_DPG, W1) )
#define afgis_dpga(L1)                      ( afgis_op_long(AFGIS_OP_DPGA, L1) )
#define afgis_panxy(W1,W2)                  ( afgis_op_word_word(AFGIS_OP_PANXY, W1, W2) )
#define afgis_wpg(W1)                       ( afgis_op_word(AFGIS_OP_WPG, W1) )
#define afgis_wpga(L1)                      ( afgis_op_long(AFGIS_OP_WPGA, L1) )

uint8_t afgis_ctexti(char *str);

typedef struct afgis_image_data
{
    WORD width;     // pixels
    WORD height;    // pixels
    WORD bpp;       // bits per pixel
    WORD pitch;     // bits per row - multiple of 16
    LONG reserved;
    void *data;     // beginning of pixel data
} afgis_image_data;

#endif
