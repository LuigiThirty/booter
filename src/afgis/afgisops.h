#pragma once

// AFGIS opcodes.

// AFGIS Variable Operations
#define AFGIS_OP_ADIV       0x0002
#define AFGIS_OP_ADVV       0x0003
#define AFGIS_OP_ANDIV      0x0006
#define AFGIS_OP_ANDVV      0x0007
#define AFGIS_OP_CPIV       0x0033
#define AFGIS_OP_CPVV       0x0034
#define AFGIS_OP_DCRV       0x0042
#define AFGIS_OP_DIVV       0x0045
#define AFGIS_OP_INCV       0x005d
#define AFGIS_OP_LDIV       0x0065
#define AFGIS_OP_LDIVL      0x0066
#define AFGIS_OP_LDMV       0x0067
#define AFGIS_OP_LDMVL      0x0068
#define AFGIS_OP_LDPCV      0x0069
#define AFGIS_OP_LDPMV      0x006a
#define AFGIS_OP_LDPMVL     0x006b
#define AFGIS_OP_LDSPV      0x006c
#define AFGIS_OP_LDVM       0x006d
#define AFGIS_OP_LDVML      0x006e
#define AFGIS_OP_LDVPM      0x006f
#define AFGIS_OP_LDVPML     0x0070
#define AFGIS_OP_LDVV       0x0071
#define AFGIS_OP_MLTV       0x0080
#define AFGIS_OP_MODV       0x0081
#define AFGIS_OP_ORIV       0x0090
#define AFGIS_OP_ORVV       0x0091
#define AFGIS_OP_POPV       0x00aa
#define AFGIS_OP_POPVARS    0x00ab
#define AFGIS_OP_PUSHV      0x00ac
#define AFGIS_OP_PUSHVARS   0x00ad
#define AFGIS_OP_SBIV       0x00cd
#define AFGIS_OP_SBVV       0x00ce
#define AFGIS_OP_SLLV       0x00e0
#define AFGIS_OP_SRLV       0x00e1
#define AFGIS_OP_XCHGPC     0x00fc
#define AFGIS_OP_XCHGSP     0x00fd
#define AFGIS_OP_XORIV      0x00fe
#define AFGIS_OP_XORVV      0x00ff

// Area Fill
#define AFGIS_OP_CIRS       0x0014
#define AFGIS_OP_CIRSV      0x0015
#define AFGIS_OP_CPFILL     0x002F
#define AFGIS_OP_CPFILLV    0x0030
#define AFGIS_OP_CPFILLO    0x0140
#define AFGIS_OP_CPFILLOV   0x0141
#define AFGIS_OP_CPFILLR    0x0031
#define AFGIS_OP_CPFILLRV   0x0032
#define AFGIS_OP_ELPS       0x004B
#define AFGIS_OP_ELPSV      0x004C
#define AFGIS_OP_PATRNMODE  0x0094
#define AFGIS_OP_PATRNMODEV 0x0095
#define AFGIS_OP_PATRNREF   0x0096
#define AFGIS_OP_PATRNREFV  0x0097
#define AFGIS_OP_PFILL      0x009A
#define AFGIS_OP_PFILLV     0x009B
#define AFGIS_OP_PFILLO     0x0142
#define AFGIS_OP_PFILLOV    0x0143
#define AFGIS_OP_PFILLR     0x009C
#define AFGIS_OP_PFILLRV    0x009D
#define AFGIS_OP_RECTS      0x00B5  // RECTS    type x0 y0 x1 y1
#define AFGIS_OP_RECTSV     0x00B6
#define AFGIS_OP_RRECTS     0x0130  // RRECTS   type x0 y0 x1 y1 xradius yradius
#define AFGIS_OP_RRECTSV    0x0131
#define AFGIS_OP_SECTS      0x00D1
#define AFGIS_OP_SECTSV     0x00D2
#define AFGIS_OP_SEGS       0x015C
#define AFGIS_OP_SEGSV      0x015d
#define AFGIS_OP_SEEDFILL   0x00d3
#define AFGIS_OP_SEEDFILLV  0x00d4
#define AFGIS_OP_STIPPLE    0x00e6
#define AFGIS_OP_STIPPLEV   0x00e7
#define AFGIS_OP_STIPPLEX   0x018a
#define AFGIS_OP_STIPPLEXV  0x018b
#define AFGIS_OP_TILE       0x00ec
#define AFGIS_OP_TILEV      0x00ed
#define AFGIS_OP_TILEX      0x018c
#define AFGIS_OP_TILEXV     0x018d

// Clipping
#define AFGIS_OP_CLIPMODE   0x0016
#define AFGIS_OP_CLIPMODEV  0x0017
#define AFGIS_OP_CLIPWIN    0x0018
#define AFGIS_OP_CLIPWINV   0x0019

// Colors/Palettes
#define AFGIS_OP_BLINK      0x0162
#define AFGIS_OP_BLINKV     0x0163
#define AFGIS_OP_BLINKON    0x0164
#define AFGIS_OP_BLINKONV   0x0165
#define AFGIS_OP_COLORB     0x001D
#define AFGIS_OP_COLORBV    0x001E
#define AFGIS_OP_COLORF     0x001F  // COLORF   long
#define AFGIS_OP_COLORFV    0x0020
#define AFGIS_OP_GETPALETTE     0x0198
#define AFGIS_OP_GETPALETTEV    0x0199
#define AFGIS_OP_R_RGB      0x0190
#define AFGIS_OP_R_RGBV     0x0191
#define AFGIS_OP_SETPALETTE     0x0112
#define AFGIS_OP_SETPALETTEV    0x0113
#define AFGIS_OP_SETRGB     0x018e
#define AFGIS_OP_SETRGBV    0x018f

// Current Position
#define AFGIS_OP_MOVETO     0x0082  // MOVETO   x y
#define AFGIS_OP_MOVETOV    0x0083
#define AFGIS_OP_MOVETOR    0x0084  // MOVETOR  x_off y_off
#define AFGIS_OP_MOVETORV   0x0085
#define AFGIS_OP_XYORG      0x0100
#define AFGIS_OP_XYORGV     0x0101

// Image Operations
#define AFGIS_OP_COPYRS     0x0029
#define AFGIS_OP_COPYRSV    0x002A
#define AFGIS_OP_COPYRSP    0x0166
#define AFGIS_OP_COPYRSPV   0x0167
#define AFGIS_OP_COPYSR     0x002B
#define AFGIS_OP_COPYSRV    0x002C
#define AFGIS_OP_COPYSRP    0x0168
#define AFGIS_OP_COPYSRPV   0x0169
#define AFGIS_OP_COPYSS     0x002D
#define AFGIS_OP_COPYSSV    0x002E
#define AFGIS_OP_COPYSSP    0x016A
#define AFGIS_OP_COPYSSPV   0x016B

// Line Drawing
#define AFGIS_OP_ARC        0x0008
#define AFGIS_OP_ARCV       0x0009
#define AFGIS_OP_ARCTIC     0x000A
#define AFGIS_OP_ARCTICV    0x000B
#define AFGIS_OP_CIR        0x0012
#define AFGIS_OP_CIRV       0x0013
#define AFGIS_OP_DASHCON    0x003C
#define AFGIS_OP_DASHCONV   0x003d
#define AFGIS_OP_DASHOFFS   0x003e
#define AFGIS_OP_DASHOFFSV  0x003f
#define AFGIS_OP_DASHPATN   0x0040
#define AFGIS_OP_DASHPATNV  0x0041
#define AFGIS_OP_ELP        0x0049
#define AFGIS_OP_ELPV       0x004a
#define AFGIS_OP_FATLNC     0x0050
#define AFGIS_OP_FATLNCV    0x0051
#define AFGIS_OP_FATLNJ     0x0052
#define AFGIS_OP_FATLNJV    0x0053
#define AFGIS_OP_FATLNW     0x0054
#define AFGIS_OP_FATLNWV    0x0055
#define AFGIS_OP_LINE       0x0074
#define AFGIS_OP_LINEV      0x0075
#define AFGIS_OP_LINECON    0x0076
#define AFGIS_OP_LINECONV   0x0077
#define AFGIS_OP_LINEPATN   0x0078
#define AFGIS_OP_LINEPATNV  0x0079
#define AFGIS_OP_LINER      0x007a
#define AFGIS_OP_LINERV     0x007b
#define AFGIS_OP_LINETO     0x007c
#define AFGIS_OP_LINETOV    0x007d
#define AFGIS_OP_LINETOR    0x007e
#define AFGIS_OP_LINETORV   0x007f
#define AFGIS_OP_PENDEF     0x0098
#define AFGIS_OP_PENDEFV    0x0099
#define AFGIS_OP_PIXEL      0x009e
#define AFGIS_OP_PIXELV     0x009f
#define AFGIS_OP_PIXELC     0x0150
#define AFGIS_OP_PIXELCV    0x0151
#define AFGIS_OP_PLINE      0x00a0
#define AFGIS_OP_PLINEV     0x00a1
#define AFGIS_OP_PLINEO     0x0144
#define AFGIS_OP_PLINEOV    0x0145
#define AFGIS_OP_PLINER     0x00a2
#define AFGIS_OP_PLINERV    0x00a3
#define AFGIS_OP_PLINES     0x0182
#define AFGIS_OP_PLINESV    0x0183
#define AFGIS_OP_PLINESR    0x0184
#define AFGIS_OP_PLINSERV   0x0185
#define AFGIS_OP_PPIXEL     0x0186
#define AFGIS_OP_PPIXELV    0x0187
#define AFGIS_OP_PPIXELO    0x0196
#define AFGIS_OP_PPIXELOV   0x0197
#define AFGIS_OP_PPIXELR    0x0194
#define AFGIS_OP_PPIXELRV   0x0195
#define AFGIS_OP_RECT       0x00b3
#define AFGIS_OP_RECTV      0x00b4
#define AFGIS_OP_RRECT      0x015e
#define AFGIS_OP_RRECTV     0x015f
#define AFGIS_OP_SECT       0x00cf
#define AFGIS_OP_SECTV      0x00d0
#define AFGIS_OP_SEG        0x0160
#define AFGIS_OP_SEGV       0x0161

// Pixel Processing
#define AFGIS_OP_BOOL       0x000C
#define AFGIS_OP_BOOLV      0x000D
#define AFGIS_OP_PMASK      0x00A8
#define AFGIS_OP_PMASKV     0x00A9
#define AFGIS_OP_TRANS      0x00EE
#define AFGIS_OP_TRANSV     0x00EF

// Program Flow
#define AFGIS_OP_CAL        0x000e
#define AFGIS_OP_CALV       0x000f
#define AFGIS_OP_CALR       0x010e
#define AFGIS_OP_CALRV      0x010f
#define AFGIS_OP_CASM       0x0010
#define AFGIS_OP_CASMV      0x0011
#define AFGIS_OP_DELAY      0x0043
#define AFGIS_OP_DELAYV     0x0044
#define AFGIS_OP_ERPT       0x004f
#define AFGIS_OP_JUMPA      0x010a
#define AFGIS_OP_JUMPAV     0x010b
#define AFGIS_OP_JUMPR      0x010c
#define AFGIS_OP_JUMPRV     0x010d
#define AFGIS_OP_RPT        0x00b7
#define AFGIS_OP_RPTV       0x00b8
#define AFGIS_OP_RTRN       0x00b9
#define AFGIS_OP_VWAIT      0x00f8

// Utilities
#define AFGIS_OP_COPYRR     0x0027
#define AFGIS_OP_COPYRRV    0x0028
#define AFGIS_OP_NOOP       0x0000
#define AFGIS_OP_R_XYLADD   0x00CB
#define AFGIS_OP_R_XYLADDV  0x00CC

// Video Page Operations
#define AFGIS_OP_CLRM       0x001A
#define AFGIS_OP_CLRPAGE    0x017A
#define AFGIS_OP_CLRPAGEV   0x017B
#define AFGIS_OP_CLRPG      0x001B
#define AFGIS_OP_CLRPGV     0x001C
#define AFGIS_OP_CLRWIN     0x0188
#define AFGIS_OP_CLRWINV    0x0189
#define AFGIS_OP_COPYPP     0x0025
#define AFGIS_OP_COPYPPV    0x0026
#define AFGIS_OP_DPAGE      0x017C
#define AFGIS_OP_DPAGEV     0x017D
#define AFGIS_OP_DPG        0x0046
#define AFGIS_OP_DPGA       0x0047
#define AFGIS_OP_DPGV       0x0048
#define AFGIS_OP_PANX       0x0116
#define AFGIS_OP_PANXV      0x0117
#define AFGIS_OP_PANY       0x0118
#define AFGIS_OP_PANYV      0x0119
#define AFGIS_OP_PANXY      0x011A
#define AFGIS_OP_PANXYV     0x011B
#define AFGIS_OP_PANXYR     0x011C
#define AFGIS_OP_PANXYRV    0x011D
#define AFGIS_OP_PANXYR     0x011C
#define AFGIS_OP_PANXYRV    0x011D
#define AFGIS_OP_WPAGEB     0x017E
#define AFGIS_OP_WPAGEBV    0x017F
#define AFGIS_OP_WPAGEB     0x017E
#define AFGIS_OP_WPAGEBV    0x017F
#define AFGIS_OP_WPAGEC     0x0180
#define AFGIS_OP_WPAGECV    0x0181
#define AFGIS_OP_WPG        0x00F9
#define AFGIS_OP_WPGA       0x00FA
#define AFGIS_OP_WPGV       0x00FB

#define AFGIS_OP_CTEXTI     0x0036  // CTEXTI   <string>
#define AFGIS_OP_CTEXTLXY   0x0038  // CTEXTLXY w_x w_y
#define AFGIS_OP_EODL       0x0001  // EODL
#define AFGIS_OP_FONT       0x0056  // FONT     font#
#define AFGIS_OP_LDIV       0x0065  // LDIV     word Vd
#define AFGIS_OP_LED        0x0072  // LED      flag

// Condition codes

#define AFGIS_CC_UC ( 0 )
#define AFGIS_CC_P  ( 1 )
#define AFGIS_CC_LS ( 2 )
#define AFGIS_CC_HI ( 3 )
#define AFGIS_CC_LT ( 4 )
#define AFGIS_CC_GE ( 5 )
#define AFGIS_CC_LE ( 6 )
#define AFGIS_CC_GT ( 7 )
#define AFGIS_CC_C  ( 8 )
#define AFGIS_CC_B  ( 8 )
#define AFGIS_CC_LO ( 8 )
#define AFGIS_CC_NC ( 9 )
#define AFGIS_CC_NB ( 9 )
#define AFGIS_CC_HS ( 9 )
#define AFGIS_CC_Z  ( 10 )
#define AFGIS_CC_EQ ( 10 )
#define AFGIS_CC_NZ ( 11 )
#define AFGIS_CC_NE ( 11 )
#define AFGIS_CC_V  ( 12 )
#define AFGIS_CC_NV ( 13 )
#define AFGIS_CC_N  ( 14 )
#define AFGIS_CC_NN ( 15 )

// Variables
typedef enum {
    AFGIS_V0,
    AFGIS_V1,
    AFGIS_V2,
    AFGIS_V3,
    AFGIS_V4,
    AFGIS_V5,
    AFGIS_V6,
    AFGIS_V7,
    AFGIS_V8,
    AFGIS_V9,
    AFGIS_V10,
    AFGIS_V11,
    AFGIS_V12,
    AFGIS_V13,
    AFGIS_V14,
    AFGIS_V15,
    AFGIS_V16,
    AFGIS_V17,
    AFGIS_V18,
    AFGIS_V19,
    AFGIS_V20,
    AFGIS_V21,
    AFGIS_V22,
    AFGIS_V23,
    AFGIS_V24,
    AFGIS_V25,
    AFGIS_V26,
    AFGIS_V27,
    AFGIS_V28,
    AFGIS_V29,
    AFGIS_V30,
    AFGIS_V31,
} AFGIS_VARIABLE;