#include "afgis.h"
#include "aigis/dlist.h"

/* 
   AFGIS functions. When calling, it is assumed that:
   - HSTADDR is a pointer to the next display list entry address.
   - INCW is enabled so the display list will auto-increment.
   - An RG750 is available at $E0000000.

   All functions return number of 16-bit words inserted into the display list.
*/

uint8_t afgis_op_implied(OPCODE opcode)
{
  //rg750_set_hstdata(opcode);
  AIGIS_DL_AddWord(opcode);
  return 1;
}

uint8_t afgis_op_long(OPCODE opcode, LONG value)
{
  // rg750_set_hstdata(opcode);
  // rg750_set_hstdata(value & 0xFFFF);
  // rg750_set_hstdata(value >> 16);

  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(value & 0xFFFF);
  AIGIS_DL_AddWord(value >> 16);

  return 3;
}

uint8_t afgis_op_word(OPCODE opcode, WORD value)
{
  // rg750_set_hstdata(opcode);
  // rg750_set_hstdata(value);

  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(value);

  return 2;
}

uint8_t afgis_op_word_word(OPCODE opcode, WORD w1, WORD w2)
{
  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(w1);
  AIGIS_DL_AddWord(w2);
  return 3;
}

uint8_t afgis_op_long_word(OPCODE opcode, LONG l1, WORD w1)
{
  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(l1 & 0xFFFF);
  AIGIS_DL_AddWord(l1 >> 16);
  AIGIS_DL_AddWord(w1);
  return 3;
}

uint8_t afgis_op_word_long(OPCODE opcode, WORD w1, LONG l1)
{
  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(w1);
  AIGIS_DL_AddWord(l1 & 0xFFFF);
  AIGIS_DL_AddWord(l1 >> 16);
  return 4;
}

uint8_t afgis_op_word_word_word(OPCODE opcode, WORD w1, WORD w2, WORD w3)
{ 
  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(w1);
  AIGIS_DL_AddWord(w2);
  AIGIS_DL_AddWord(w3);
  return 4;
}

uint8_t afgis_op_word_word_long(OPCODE opcode, WORD w1, WORD w2, LONG l1)
{ 
  // rg750_set_hstdata(opcode);
  // rg750_set_hstdata(w1);
  // rg750_set_hstdata(w2);
  // rg750_set_hstdata(l1 & 0xFFFF);
  // rg750_set_hstdata(l1 >> 16);

  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(w1);
  AIGIS_DL_AddWord(w2);
  AIGIS_DL_AddWord(l1 & 0xFFFF);
  AIGIS_DL_AddWord(l1 >> 16);

  return 5;
}

uint8_t afgis_op_long_word_word(OPCODE opcode, LONG l1, WORD w1, WORD w2)
{
  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(l1 & 0xFFFF);
  AIGIS_DL_AddWord(l1 >> 16);
  AIGIS_DL_AddWord(w1);
  AIGIS_DL_AddWord(w2);

  return 5;
}

uint8_t afgis_op_long_long_long(OPCODE opcode, LONG l1, LONG l2, LONG l3)
{ 
  // rg750_set_hstdata(opcode);
  // rg750_set_hstdata(l1 & 0xFFFF);
  // rg750_set_hstdata(l1 >> 16);
  // rg750_set_hstdata(l2 & 0xFFFF);
  // rg750_set_hstdata(l2 >> 16);
  // rg750_set_hstdata(l3 & 0xFFFF);
  // rg750_set_hstdata(l3 >> 16);

  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(l1 & 0xFFFF);
  AIGIS_DL_AddWord(l1 >> 16);
  AIGIS_DL_AddWord(l2 & 0xFFFF);
  AIGIS_DL_AddWord(l2 >> 16);
  AIGIS_DL_AddWord(l3 & 0xFFFF);
  AIGIS_DL_AddWord(l3 >> 16);
  return 7;
}

uint8_t afgis_op_word_word_word_word(OPCODE opcode, WORD w1, WORD w2, WORD w3, WORD w4)
{ 
  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(w1);
  AIGIS_DL_AddWord(w2);
  AIGIS_DL_AddWord(w3);
  AIGIS_DL_AddWord(w4);
  return 5;
}

uint8_t afgis_op_word_word_word_word_word(OPCODE opcode, WORD w1, WORD w2, WORD w3, WORD w4, WORD w5)
{
  // rg750_set_hstdata(opcode);
  // rg750_set_hstdata(w1);
  // rg750_set_hstdata(w2);
  // rg750_set_hstdata(w3);
  // rg750_set_hstdata(w4);
  // rg750_set_hstdata(w5);

  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(w1);
  AIGIS_DL_AddWord(w2);
  AIGIS_DL_AddWord(w3);
  AIGIS_DL_AddWord(w4);
  AIGIS_DL_AddWord(w5);
  return 6;
}

uint8_t afgis_op_word_word_word_word_long(OPCODE opcode, WORD w1, WORD w2, WORD w3, WORD w4, LONG l1)
{
  // rg750_set_hstdata(opcode);
  // rg750_set_hstdata(w1);
  // rg750_set_hstdata(w2);
  // rg750_set_hstdata(w3);
  // rg750_set_hstdata(w4);
  // rg750_set_hstdata(w5);

  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(w1);
  AIGIS_DL_AddWord(w2);
  AIGIS_DL_AddWord(w3);
  AIGIS_DL_AddWord(w4);
  AIGIS_DL_AddWord(l1 & 0xFFFF);
  AIGIS_DL_AddWord(l1 >> 16);
  return 7;
}

uint8_t afgis_op_word_word_word_word_word_word(OPCODE opcode, WORD w1, WORD w2, WORD w3, WORD w4, WORD w5, WORD w6)
{
  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(w1);
  AIGIS_DL_AddWord(w2);
  AIGIS_DL_AddWord(w3);
  AIGIS_DL_AddWord(w4);
  AIGIS_DL_AddWord(w5);
  AIGIS_DL_AddWord(w6);
  return 7;
}

uint8_t afgis_op_word_word_word_word_word_word_word(OPCODE opcode, WORD w1, WORD w2, WORD w3, WORD w4, WORD w5, WORD w6, WORD w7)
{
  AIGIS_DL_AddWord(opcode);
  AIGIS_DL_AddWord(w1);
  AIGIS_DL_AddWord(w2);
  AIGIS_DL_AddWord(w3);
  AIGIS_DL_AddWord(w4);
  AIGIS_DL_AddWord(w5);
  AIGIS_DL_AddWord(w6);
  AIGIS_DL_AddWord(w7);
  return 8;
}

uint8_t afgis_ctexti(char *str)
{
  // Insert a string into the display list.
  // Each word of the string is byte-swapped.
  // It is null-terminated.

  /*
  uint8_t words = 1;

  rg750_set_hstdata(AFGIS_OP_CTEXTI);
  for(int i=0; i<strlen(str); i+=2)
    {
      if(str[i] == 0)
	break;
      uint16_t swapped_word = ((str[i+1] << 8) | str[i]);
      rg750_set_hstdata(swapped_word);
      words++;
    }
  rg750_set_hstdata(0x0000);
  words++;
  */

  uint8_t words = 1;

  AIGIS_DL_AddWord(AFGIS_OP_CTEXTI);
  for(int i=0; i<strlen(str); i+=2)
    {
      if(str[i] == 0)
	break;
      uint16_t swapped_word = ((str[i+1] << 8) | str[i]);
      AIGIS_DL_AddWord(swapped_word);
      words++;
    }
  AIGIS_DL_AddWord(0x0000);
  words++;

  return words;
}