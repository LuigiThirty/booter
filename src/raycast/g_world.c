#include "g_world.h"
#include "raycast/r_txdata.h"
#include "raycast/w_wadmgr.h"
#include "json.h"

struct GameWorldMap loaded_map;

void G_LoadMap(char *lumpname)
{
    struct WAD_Directory *lump;
    void *lump_data;
    cJSON *json;
    const char *error_ptr;

    printf("G_LoadMap: Loading map from lump '%s'\n", lumpname);
    lump = W_GetLump(lumpname);
    lump_data = W_GetLumpDataPtr(lump);

    printf("G_LoadMap: Lump data is at %08X\n", lump_data);

    json = cJSON_Parse(lump_data);

    if (json == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            printf("JSON: Error\n", error_ptr);
        }
    }
    else
    {
        const cJSON *mapname = cJSON_GetObjectItem(json, "map_name");
        const cJSON *size_x = cJSON_GetObjectItem(json, "size_x");
        const cJSON *size_y = cJSON_GetObjectItem(json, "size_y");
        const cJSON *map_tiles = cJSON_GetObjectItem(json, "tiles");
        const cJSON *map_tile;

        int tile_index;

        printf("* Map: '%s'\n", mapname->valuestring);
        printf("* Map size: %d x %d (%d tiles)\n", size_x->valueint, size_y->valueint, size_x->valueint*size_y->valueint);

        tile_index = 0;

        cJSON_ArrayForEach(map_tile, map_tiles)
        {
            const cJSON *tile_type = cJSON_GetObjectItem(map_tile, "type");
            const cJSON *tile_texture;

            //printf("Tile %d: type %d\n", tile_index, tile_type->valueint);

            loaded_map.tiles[tile_index].tile_type = tile_type->valueint;

            if(tile_type->valueint == TILE_WALL)
            {
                tile_texture = cJSON_GetObjectItem(map_tile, "texture");
                loaded_map.tiles[tile_index].texture_lump_num = TEX_GetTextureIDByName(tile_texture->valuestring);

                //printf("Tile %d: wall, texture %s (id %d)\n", tile_index, tile_texture->valuestring, loaded_map.tiles[tile_index].texture_lump_num);
            }

            tile_index++;

            error_ptr = cJSON_GetErrorPtr();
            if (error_ptr != NULL)
            {
                printf("JSON: Error\n", error_ptr);
            }
        }
    }
        
}