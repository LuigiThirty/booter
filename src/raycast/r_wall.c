#include "raycast/r_wall.h"

void draw_solid_wall_slice(uint16_t slice_x, uint16_t height, uint8_t color)
{
    ForeColor(color);
    Line(AIGIS_LINE_SOLID, slice_x, MAX(121, (SCREEN_HEIGHT>>1) - (height>>1)), slice_x, MIN(360, (SCREEN_HEIGHT>>1) + (height>>1)));
}