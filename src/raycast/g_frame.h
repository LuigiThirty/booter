#pragma once

#include <stdio.h>
#include <stdint.h>

#include "aigis/aigis.h"
#include "raycast/w_wadmgr.h"
#include "raycast/r_raycst.h"
#include "raycast/r_txdata.h"
#include "raycast/r_wall.h"
#include "raycast/i_inputs.h"

extern struct R_RaycasterState raycasterState;

void RC_Initialize();
void RC_FrameLoop();

void RC_RedrawBG();