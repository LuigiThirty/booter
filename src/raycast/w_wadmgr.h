#ifndef W_WADMGR_H
#define W_WADMGR_H

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

extern uint8_t *_binary_data_bundle_wad_start;

uint32_t W_SwapEndianness(uint32_t in);
uint16_t W_SwapEndianness16(uint16_t in);

#pragma pack(1)
struct WAD_Directory {
    void *data;
    uint32_t size;      /* in bytes */
    char lump_name[8];  /* might not be null-terminated! */
};

struct WAD_Header {
    char magic[4];
    uint32_t directory_entries;
    struct WAD_Directory *directory_start;
};
#pragma pack()

void W_LoadBundle();
void *W_GetWadBase();
uint16_t W_GetLumpCount();
void *W_GetLumpDataPtr(struct WAD_Directory *lump);
void *W_ConvertWadPtrToGlobalPtr(void *data);
struct WAD_Directory *W_GetLump(char *lumpname);

#endif