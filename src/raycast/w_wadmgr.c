#include "raycast/w_wadmgr.h"

struct WAD_Header header;
struct WAD_Directory *directory;

uint32_t W_SwapEndianness(uint32_t in)
{
    return  ((in>>24)&0xff)    | // move byte 3 to byte 0
            ((in<<8)&0xff0000) | // move byte 1 to byte 2
            ((in>>8)&0xff00)   | // move byte 2 to byte 1
            ((in<<24)&0xff000000); // byte 0 to byte 3
}

uint16_t W_SwapEndianness16(uint16_t in)
{
    return (in >> 8)|(in << 8);
}

void W_LoadBundle()
{
    int i=0;
    printf("W_LoadBundle: BUNDLE.WAD is at %08X\n", &_binary_data_bundle_wad_start);
    memcpy(&header, &_binary_data_bundle_wad_start, 12);
    header.directory_entries = W_SwapEndianness(header.directory_entries);
    header.directory_start = (struct WAD_Directory *)W_SwapEndianness((uint32_t)header.directory_start);

    printf("W_LoadBundle: BUNDLE contains %d entries at offset %08X.\n", header.directory_entries, (uint32_t)&_binary_data_bundle_wad_start + (uint32_t)header.directory_start);

    directory = (struct WAD_Directory *)((uint32_t)&_binary_data_bundle_wad_start + (uint32_t)header.directory_start);

    for(i=0; i<header.directory_entries; i++)
    {
        char lump[10];
        memcpy(lump, directory[i].lump_name, 8);
        lump[8] = 0;
        printf("%03d - %s\n", i, lump);        
    }
}

void *W_GetWadBase()
{
    return &_binary_data_bundle_wad_start;
}

uint16_t W_GetLumpCount()
{
    return header.directory_entries;
}

void *W_GetLumpDataPtr(struct WAD_Directory *lump)
{
    /* Returns an absolute pointer to the lump data in ROM. */
    return (void *)((uint32_t)W_GetWadBase() + (W_SwapEndianness((uint32_t)lump->data)));
}

void *W_ConvertWadPtrToGlobalPtr(void *data)
{
    return (void *)((uint32_t)W_GetWadBase() + ((uint32_t)data)); 
}

struct WAD_Directory *W_GetLump(char *lumpname)
{
    int i;
    char temp[9];

    int result;

    //printf("W_GetLump: getting lump %s\n", lumpname);

    memset(temp, 0, 9);

    for(i=0; i<header.directory_entries; i++)
    {
        memcpy(temp, directory[i].lump_name, 8);
        if(strcmp(lumpname, temp) == 0)
        {
            return &directory[i];
        }
    }

    return NULL;
}