#include "raycast/g_frame.h"

struct R_RaycasterState raycasterState;
FIXED zbuffer[640];

void RC_Initialize()
{
    // Initialize the raycaster.
    W_LoadBundle();
    TEX_LoadAllTexturesFromWad(texture_lookup, MAX_TEXTURES);

	printf("Initializing player state\n");
	raycasterState.player_position.x = FX_FROMFLOAT(12.0);  raycasterState.player_position.y = FX_FROMFLOAT(12.0);
	raycasterState.player_direction.x = FX_FROMFLOAT(-1.0); raycasterState.player_direction.y = FX_FROMFLOAT(0.0);
	raycasterState.camera_plane.x = FX_FROMFLOAT(0.0);      raycasterState.camera_plane.y = FX_FROMFLOAT(0.66);

	raycasterState.zbuffer = zbuffer;

    R_CalculateRotationSpeeds(&rotationTable, 0.125f);
}

void RC_FrameLoop()
{
    RC_RedrawBG();
    I_MovePlayer(&raycasterState, &rotationTable);
    RC_Cast(&raycasterState);
}

void RC_RedrawBG()
{
	struct Rect r;

    ForeColor(0);
	SetRect(&r, 160, 120, 480, 240);
	PaintRect(&r, 0);

    ForeColor(6);
	SetRect(&r, 160, 240, 480, 360);
	PaintRect(&r, 0);

    ForeColor(15);
	SetRect(&r, 159, 120, 480, 361);
	FrameRect(&r);
}