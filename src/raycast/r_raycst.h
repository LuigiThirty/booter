#ifndef RAYCAST_H
#define RAYCAST_H

#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include "g_world.h"
#include "aigis/aigis.h"
#include "raycast/r_wall.h"

struct RotationSinCos {
    /* These values are calculated from rotSpeed. */
    FIXED left_cos_rotSpeed, left_negative_cos_rotSpeed, left_sin_rotSpeed, left_negative_sin_rotSpeed;	
    FIXED right_cos_rotSpeed, right_negative_cos_rotSpeed, right_sin_rotSpeed, right_negative_sin_rotSpeed;	
};

struct R_RaycasterState {
    struct FX_VECTOR2 player_position; 	/* player position */
    struct FX_VECTOR2 player_direction;	/* player direction */
    struct FX_VECTOR2 camera_plane;		/* camera plane */

    struct FX_VECTOR2 camera;
	struct FX_VECTOR2 ray_direction;
	struct FX_VECTOR2 delta_distance;

    /* Line segment */
    FIXED fxLineHeight;
	FIXED drawStart, drawEnd;

    /* Length of ray from current pos to next X or Y side */
    struct FX_VECTOR2 side_distance;

    struct FX_VECTOR2 map_position;	/* map coordinates */
	FIXED fx_perpWallDist;

    struct FX_VECTOR2 step;
	int hit;
	int side;

    FIXED *zbuffer; /* distance to wall for each column */
};

extern struct RotationSinCos rotationTable;

void R_CalculateRotationSpeeds(struct RotationSinCos *table, float rotSpeed);

void R_DDA(struct R_RaycasterState *r_state);
void RC_Cast(struct R_RaycasterState *);

#endif