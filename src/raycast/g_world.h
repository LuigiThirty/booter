#ifndef G_WORLD_H
#define G_WORLD_H

#include <stdint.h>
#include <stdbool.h>

enum G_TILETYPE {
    TILE_FLOOR,
    TILE_WALL
};

struct TileDef {
    enum G_TILETYPE tile_type;

    /* If TILE_WALL: The lump ID of the texture used for this tile. */
    int16_t texture_lump_num;

};

struct GameWorldMap {
    struct TileDef tiles[64*64];
};

extern struct GameWorldMap loaded_map;

void G_LoadMap(char *lumpname);

#endif