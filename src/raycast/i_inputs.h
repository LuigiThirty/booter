#ifndef I_INPUTS_H
#define I_INPUTS_H

#include <stdlib.h>
#include <stdint.h>

#include "raycast/r_raycst.h"
#include "aigis/aigis.h"

#define INPUT_P1    0xF1000000

#define I_BIT_UP    0x01
#define I_BIT_DOWN  0x02
#define I_BIT_LEFT  0x04
#define I_BIT_RIGHT 0x08

void I_MovePlayer(struct R_RaycasterState *r_state, struct RotationSinCos *rotation_table);

#endif