#include "raycast/r_txdata.h"
#include "raycast/w_wadmgr.h"

struct Texture texture_lookup[MAX_TEXTURES];

/* TODO: load all lumps between TEXSTART and TEXEND */

void TEX_LoadFromLump(struct Texture *texture, char *lumpname)
{
    struct WAD_Directory *lump;
    struct BMPHeader *bmp_header;

    printf("Loading texture from lump %s\n", lumpname);

    lump = W_GetLump(lumpname);
    if(lump == NULL)
    {
        printf("*** ASSET ERROR: Lump %s not found\n", lumpname);
        while(1) {};
    }
    bmp_header = (struct BMPHeader *)W_GetLumpDataPtr(lump);
    printf("bmp_header %08X\n", bmp_header);

    texture->data = (void *)((uint32_t)W_GetLumpDataPtr(lump) + (uint32_t)0x436);
    texture->size.x = W_SwapEndianness(bmp_header->image_width);
    texture->size.y = W_SwapEndianness(bmp_header->image_height);
    strcpy(texture->lumpname, lumpname);
    printf("Created texture '%s' with size (%d,%d), image at %08X\n", lumpname, texture->size.x, texture->size.y, texture->data);
}

/*
void TEX_Draw(struct FGState *state, struct Texture *tex, struct XY *coords)
{
    copy_image(state->back_buffer, tex->data, tex->size.x, tex->size.y, coords->x, coords->y);
}

void TEX_DrawScaled(struct FGState *state, struct Texture *tex, struct XY *coords, uint16_t dest_size_x, uint16_t dest_size_y)
{
    copy_image_scaled(state->back_buffer, tex->data, tex->size.x, tex->size.y, coords->x, coords->y, dest_size_x, dest_size_y);
}
*/

int TEX_GetTextureIDByName(char *lumpname)
{
    int i;

    for(i=0; i<MAX_TEXTURES; i++)
    {
        if(strcmp(lumpname, texture_lookup[i].lumpname) == 0)
        {
            return i;
        }
    }

    printf("*** TEX_GetTextureIDByName: Tried to look up nonexistent texture '%s'\n", lumpname);
    while(1) {};
}

void TEX_LoadAllTexturesFromWad(struct Texture *texture_array, uint16_t max_texture_count)
{
    struct WAD_Directory *dir_entry;
    char lumpname_buffer[9];
    int keep_going = 1;
    int texture_count = 0;

    memset(lumpname_buffer, 0, 9);

    /* Advance in the directory until we find TEXEND. */
    dir_entry = W_GetLump("TEXSTART");

    while(1)
    {
        dir_entry++;

        memcpy(lumpname_buffer, dir_entry->lump_name, 8);
        if(strcmp(lumpname_buffer, "TEXEND") == 0)
        {
            break;
        }

        TEX_LoadFromLump(texture_array++, lumpname_buffer);

        if(texture_count++ == max_texture_count) { printf("*** TEX_LoadAllTexturesFromWad: Too many textures!\n"); while (1) {} }
    }

    printf("TEX_LoadAllTexturesFromWad: Loaded %d textures\n", texture_count);
}