#include "raycast/i_inputs.h"

void I_MovePlayer(struct R_RaycasterState *r_state, struct RotationSinCos *rotation_table)
{
    uint8_t input_byte = I_BIT_RIGHT; //I_GetP1InputByte();
    struct FX_VECTOR2 old_player_direction;
    struct FX_VECTOR2 old_camera_plane;

    old_player_direction.x = r_state->player_direction.x;
    old_camera_plane.x = r_state->camera_plane.x;

    if((input_byte & I_BIT_LEFT) > 0)
		{
			/* Input: P1 left */
			r_state->player_direction.x = FX_MUL(r_state->player_direction.x, rotation_table->left_negative_cos_rotSpeed) - (FX_MUL(r_state->player_direction.y, rotation_table->left_negative_sin_rotSpeed));
			r_state->player_direction.y = FX_MUL(old_player_direction.x, rotation_table->left_negative_sin_rotSpeed) + (FX_MUL(r_state->player_direction.y, rotation_table->left_negative_cos_rotSpeed));
			r_state->camera_plane.x = FX_MUL(r_state->camera_plane.x, rotation_table->left_negative_cos_rotSpeed) - (FX_MUL(r_state->camera_plane.y, rotation_table->left_negative_sin_rotSpeed));
			r_state->camera_plane.y = FX_MUL(old_camera_plane.x, rotation_table->left_negative_sin_rotSpeed) + (FX_MUL(r_state->camera_plane.y, rotation_table->left_negative_cos_rotSpeed));
		}
		if((input_byte & I_BIT_RIGHT) > 0)
		{
			/* Input: P1 right */
			r_state->player_direction.x = FX_MUL(r_state->player_direction.x, rotation_table->right_negative_cos_rotSpeed) - (FX_MUL(r_state->player_direction.y, rotation_table->right_negative_sin_rotSpeed));
			r_state->player_direction.y = FX_MUL(old_player_direction.x, rotation_table->right_negative_sin_rotSpeed) + (FX_MUL(r_state->player_direction.y, rotation_table->right_negative_cos_rotSpeed));
			r_state->camera_plane.x = FX_MUL(r_state->camera_plane.x, rotation_table->right_negative_cos_rotSpeed) - (FX_MUL(r_state->camera_plane.y, rotation_table->right_negative_sin_rotSpeed));
			r_state->camera_plane.y = FX_MUL(old_camera_plane.x, rotation_table->right_negative_sin_rotSpeed) + (FX_MUL(r_state->camera_plane.y, rotation_table->right_negative_cos_rotSpeed));
		}
		if((input_byte & I_BIT_UP) > 0)
		{
			/* Input: P1 forward */
			FIXED new_x, new_y;

			new_x = r_state->player_position.x + FX_MUL(r_state->player_direction.x, 0x00004000);
			new_y = r_state->player_position.y + FX_MUL(r_state->player_direction.y, 0x00004000);

			/* Don't adjust the player position unless the destination tile is passable. */
            /*
			if(worldmap[FX_TOINT(new_x)][FX_TOINT(new_y)] == 0)
			{
				r_state->player_position.x = new_x;
				r_state->player_position.y = new_y;
			}
            */
		}
		if((input_byte & I_BIT_DOWN) > 0)
		{
			/* Input: P1 backward */
			FIXED new_x, new_y;

			new_x = r_state->player_position.x - FX_MUL(r_state->player_direction.x, 0x00004000);
			new_y = r_state->player_position.y - FX_MUL(r_state->player_direction.y, 0x00004000);

			/* Don't adjust the player position unless the destination tile is passable. */
            /*
			if(worldmap[FX_TOINT(new_x)][FX_TOINT(new_y)] == 0)
			{
				r_state->player_position.x = new_x;
				r_state->player_position.y = new_y;
			}
            */
		}
}