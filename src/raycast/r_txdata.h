#ifndef R_SPRITE_H
#define R_SPRITE_H

#include "aigis/types.h"
#include "aigis/xy.h"

struct Texture {
    char lumpname[10];
    struct XY size;
    uint8_t *data;
};

#define MAX_TEXTURES 64

extern struct Texture texture_lookup[MAX_TEXTURES];

extern void copy_image(void *buffer, uint8_t *source, uint16_t size_x, uint16_t size_y, int16_t dest_x, int16_t dest_y);

void TEX_LoadFromLump(struct Texture *texture, char *lumpname);
void TEX_LoadAllTexturesFromWad(struct Texture *texture_array, uint16_t max_texture_count);

int TEX_GetTextureIDByName(char *lumpname);
/*
void TEX_Draw(struct FGState *state, struct Texture *tex, struct XY *coords);
void TEX_DrawScaled(struct FGState *state, struct Texture *tex, struct XY *coords, uint16_t dest_size_x, uint16_t dest_size_y);
*/

#endif