#include "raycast/r_raycst.h"

#define MAP_WIDTH 32
#define MAP_HEIGHT 32

struct RotationSinCos rotationTable;

extern struct GameWorldMap loaded_map;

void R_CalculateRotationSpeeds(struct RotationSinCos *table, float rotSpeed)
{
	printf("Populating rotation table: rotSpeed %f\n", rotSpeed);

	table->right_cos_rotSpeed = FX_FROMFLOAT(cosf(rotSpeed));
	table->right_sin_rotSpeed = FX_FROMFLOAT(sinf(rotSpeed));
	table->right_negative_cos_rotSpeed = FX_FROMFLOAT(cosf(-rotSpeed));
	table->right_negative_sin_rotSpeed = FX_FROMFLOAT(sinf(-rotSpeed));

	table->left_cos_rotSpeed = FX_FROMFLOAT(cosf(-rotSpeed));
	table->left_sin_rotSpeed = FX_FROMFLOAT(sinf(-rotSpeed));
	table->left_negative_cos_rotSpeed = FX_FROMFLOAT(cosf(rotSpeed));
	table->left_negative_sin_rotSpeed = FX_FROMFLOAT(sinf(rotSpeed));

	printf("RIGHT_COS_ROTSPEED %08X\n", table->right_cos_rotSpeed);
	printf("RIGHT_SIN_ROTSPEED %08X\n", table->right_sin_rotSpeed);
}

void R_DDA(struct R_RaycasterState *r_state)
{
	bool hit = 0;

	register FIXED r_fxSideDistX 	= r_state->side_distance.x;
	register FIXED r_fxSideDistY 	= r_state->side_distance.y;
	register FIXED r_fxDeltaDistX	= r_state->delta_distance.x;
	register FIXED r_fxDeltaDistY 	= r_state->delta_distance.y;
	register FIXED r_fx_mapX 		= r_state->map_position.x;
	register FIXED r_fx_mapY 		= r_state->map_position.y;
	register FIXED r_fx_stepX 		= r_state->step.x;
	register FIXED r_fx_stepY 		= r_state->step.y;

	while (hit == 0)
	{
		/* jump to next map square, OR in x-direction, OR in y-direction */
		if (r_fxSideDistX < r_fxSideDistY)
		{
			r_fxSideDistX += r_fxDeltaDistX;
			r_fx_mapX += r_fx_stepX;
			r_state->side = 0;
		}
		else
		{
			r_fxSideDistY += r_fxDeltaDistY;
			r_fx_mapY += r_fx_stepY;
			r_state->side = 1;
		}
		/* Check if ray has hit a wall. */
		if (loaded_map.tiles[(FX_TOINT(r_fx_mapY)*MAP_WIDTH)+FX_TOINT(r_fx_mapX)].tile_type > 0) hit = 1;
	}

	r_state->side_distance.x 	= r_fxSideDistX;
	r_state->side_distance.y 	= r_fxSideDistY;
	r_state->delta_distance.x 	= r_fxDeltaDistX;
	r_state->delta_distance.y 	= r_fxDeltaDistY;
	r_state->map_position.x		= r_fx_mapX;
	r_state->map_position.y		= r_fx_mapY;
	r_state->step.x 			= r_fx_stepX;
	r_state->step.y 			= r_fx_stepY;
}

void RC_Cast(struct R_RaycasterState *raycasterState)
{
	const uint16_t VISIBLE_HEIGHT = SCREEN_HEIGHT - 240;
	const uint16_t VISIBLE_WIDTH = SCREEN_WIDTH - 320;
	const uint16_t HALF_VISIBLE_HEIGHT = VISIBLE_HEIGHT/2;
	const uint16_t HALF_VISIBLE_WIDTH = VISIBLE_WIDTH/2;

	const FIXED FX_VISIBLE_HEIGHT = FX_FROMINT(VISIBLE_HEIGHT);
	const FIXED FX_VISIBLE_WIDTH = FX_FROMINT(VISIBLE_WIDTH);
	const FIXED FX_HALF_VISIBLE_HEIGHT = FX_FROMINT(HALF_VISIBLE_HEIGHT);
	const FIXED FX_HALF_VISIBLE_WIDTH = FX_FROMINT(HALF_VISIBLE_WIDTH);

	/* Line segment */
	struct XY from, to;
	uint16_t color;
	FIXED drawStart, drawEnd;

	struct Rect r;

	const int leftmost_ray = 0;
	const int fx_leftmost_ray = FX_FROMINT(fx_leftmost_ray);

	FIXED forward_vector_x, forward_vector_y;

	FIXED wall_intercept_x;
	uint8_t texel_x;
	FIXED texel_y_step;
	FIXED texture_position;

	uint8_t wall_height;

	FIXED sprite_pos_x, sprite_pos_y;

	struct XY tex_dest;

	FIXED sprite_distances[16];

	FIXED wall_frac_x;
	FIXED wall_texel_x;

	int texture_id;

	sprite_pos_x = FX_FROMFLOAT(22.0);
	sprite_pos_y = FX_FROMFLOAT(12.0);

	int ray_x;

	for(ray_x=leftmost_ray; ray_x<320; ray_x++)
	{
		raycasterState->camera.x = FX_DIV(FX_FROMINT(ray_x << 1), FX_FROMINT(VISIBLE_WIDTH)) - 0x00010000; /* X coordinate in camera space */
		raycasterState->camera.y = raycasterState->camera.x;

		/* We only want the integer parts of these values. */
		raycasterState->map_position.x = raycasterState->player_position.x & 0xFFFF0000; 	/* map coordinate, X */
		raycasterState->map_position.y = raycasterState->player_position.y & 0xFFFF0000;	/* map coordinate, Y */

		/* length of ray from one X or Y side to next X or Y side */
		raycasterState->ray_direction.x = FX_ADD(raycasterState->player_direction.x, FX_MUL(raycasterState->camera_plane.x, raycasterState->camera.x));
		raycasterState->ray_direction.y = FX_ADD(raycasterState->player_direction.y, FX_MUL(raycasterState->camera_plane.y, raycasterState->camera.y));
		raycasterState->delta_distance.x = (raycasterState->ray_direction.y == 0) ? 0 : ((raycasterState->ray_direction.x == 0) ? 1 : abs(FX_DIV(0x00010000, raycasterState->ray_direction.x)));
		raycasterState->delta_distance.y = (raycasterState->ray_direction.x == 0) ? 0 : ((raycasterState->ray_direction.y == 0) ? 1 : abs(FX_DIV(0x00010000, raycasterState->ray_direction.y)));

		/* Calculate step and initial sideDist. */
		if (raycasterState->ray_direction.x < 0)
		{
			raycasterState->step.x = 0xFFFF0000;
			raycasterState->side_distance.x = FX_MUL( FX_SUB(raycasterState->player_position.x, raycasterState->map_position.x), raycasterState->delta_distance.x );
		}
		else
		{
			raycasterState->step.x = 0x00010000;
			raycasterState->side_distance.x = FX_MUL( FX_SUB(raycasterState->map_position.x + 0x00010000, raycasterState->player_position.x), raycasterState->delta_distance.x );
		}
		if (raycasterState->ray_direction.y < 0)
		{
			raycasterState->step.y = 0xFFFF0000;
			raycasterState->side_distance.y = FX_MUL( FX_SUB(raycasterState->player_position.y, raycasterState->map_position.y), raycasterState->delta_distance.y );
		}
		else
		{
			raycasterState->step.y = 0x00010000;
			raycasterState->side_distance.y = FX_MUL( FX_SUB(raycasterState->map_position.y + 0x00010000, raycasterState->player_position.y), raycasterState->delta_distance.y );
		}

		/* Perform DDA. */
		R_DDA(raycasterState);

		/* Calculate distance projected on camera direction (Euclidean distance will give fisheye effect!) */
		if (raycasterState->side == 0) raycasterState->fx_perpWallDist = FX_DIV(raycasterState->map_position.x - raycasterState->player_position.x + FX_DIV(0x00010000 - raycasterState->step.x, 0x00020000), raycasterState->ray_direction.x);
		else		   				   raycasterState->fx_perpWallDist = FX_DIV(raycasterState->map_position.y - raycasterState->player_position.y + FX_DIV(0x00010000 - raycasterState->step.y, 0x00020000), raycasterState->ray_direction.y);

		/* Calculate height of line to draw on screen */
		raycasterState->fxLineHeight = FX_DIV(FX_VISIBLE_HEIGHT, raycasterState->fx_perpWallDist);

		/* calculate lowest and highest pixel to fill in current stripe */
		drawStart = -FX_DIV(raycasterState->fxLineHeight, 0x00020000) + FX_HALF_VISIBLE_HEIGHT;
		drawEnd = FX_DIV(raycasterState->fxLineHeight, 0x00020000) + FX_HALF_VISIBLE_HEIGHT;

		if(drawStart < 0x00290000) drawStart = 0x00290000;
		if(drawEnd >= FX_VISIBLE_HEIGHT+1) drawEnd = FX_VISIBLE_HEIGHT;

		/* Z Buffer for wall distances */
		/*
		raycasterState->zbuffer[ray_x] = raycasterState->fx_perpWallDist;
		raycasterState->zbuffer[ray_x+1] = raycasterState->fx_perpWallDist;
		*/

		//texture_id = worldmap[FX_TOINT(raycasterState->map_position.y)][FX_TOINT(raycasterState->map_position.x)];

		if(raycasterState->side == 0) wall_frac_x = raycasterState->player_position.y + FX_MUL(raycasterState->fx_perpWallDist, raycasterState->ray_direction.y);
		else						  wall_frac_x = raycasterState->player_position.x + FX_MUL(raycasterState->fx_perpWallDist, raycasterState->ray_direction.x);
		wall_frac_x = wall_frac_x & 0x0000FFFF;	/* only care about the fractional coordinate */

		wall_texel_x = FX_MUL(wall_frac_x, 0x00400000);
		if(raycasterState->side == 0 && raycasterState->ray_direction.x > 0) wall_texel_x = 0x00400000 - wall_texel_x - 0x00010000;
		if(raycasterState->side == 1 && raycasterState->ray_direction.y < 0) wall_texel_x = 0x00400000 - wall_texel_x - 0x00010000;

		draw_solid_wall_slice(ray_x+160, FX_TOINT(drawEnd - drawStart), loaded_map.tiles[(FX_TOINT(raycasterState->map_position.y)*MAP_WIDTH)+FX_TOINT(raycasterState->map_position.x)].tile_type);

		/*
		draw_textured_wall_slice(fg_state.back_buffer, texture_lookup[texture_id].data, FX_TOINT(wall_texel_x), texture_lookup[texture_id].size.y, ray_x+41, FX_TOINT(drawStart), FX_TOINT(drawEnd - drawStart));
		draw_textured_wall_slice(fg_state.back_buffer, texture_lookup[texture_id].data, FX_TOINT(wall_texel_x), texture_lookup[texture_id].size.y, ray_x+42, FX_TOINT(drawStart), FX_TOINT(drawEnd - drawStart));
		*/
	}
}
