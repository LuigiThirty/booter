#include "rg750.h"
#include "mvme/vmedma.h"

void rg750_acknowledge_interrupt()
{
  uint16_t data = rg750_get_hstctl();
  data = data & 0xFF7F; // Clear bit 7 of HSTCTL.
  rg750_set_hstctl(data);
}

uint8_t rg750_get_interrupt_num()
{
  uint16_t data = rg750_get_hstctl();
  return (uint8_t)((data >> 4) & 7);
}

void rg750_set_hstaddr(uint16_t *address)
{
  MMIO16(RG750_HSTADRL) = (uint16_t)((uint32_t)address & 0xFFFF);
  MMIO16(RG750_HSTADRH) = (uint16_t)((uint32_t)address >> 16);
}

uint16_t *rg750_get_hstaddr()
{
  uint32_t data;

  data = MMIO16(RG750_HSTADRL);
  data |= (MMIO16(RG750_HSTADRH) << 16);

  return (uint16_t *)data;
}

uint16_t rg750_get_hstdata()
{
  return MMIO16(RG750_HSTDATA);
}

uint16_t rg750_get_hstctl()
{
  return MMIO16(RG750_HSTCTL);
}

void rg750_set_hstctl(uint16_t data)
{
  //printf("RG750: Setting HSTCTL to %04X\n", data);
  MMIO16(RG750_HSTCTL) = data;
}

uint32_t rg750_get_long(uint16_t *address)
{
  uint32_t data;

  rg750_set_hstaddr(address);
  data = rg750_get_hstdata();
  
  address = address + 2;
  rg750_set_hstaddr(address);
  data |= (rg750_get_hstdata() << 16);

  return data;
}

void rg750_reset()
{
  // Send an NMI to the RG750, resetting it
  // to the power-up condition.
  
  // Disable DMA.
  MMIO32(L2V_DMAC1_REG) = (MMIO32(L2V_DMAC1_REG) & 0xFFFFFF00);

  uint16_t ctl = rg750_get_hstctl();
  ctl |= RG750_CTL_NMI;
  ctl &= 0xFFF0; // MSGIN = 0
  rg750_set_hstctl(ctl);  

  // clear NMI
  ctl = 0x0800;
  rg750_set_hstctl(ctl);  
}

void rg750_enable_incw()
{
  // Enable auto-increment on writes.
  uint16_t data = MMIO16(RG750_HSTCTL);
  data |= RG750_CTL_INCW;
  MMIO16(RG750_HSTCTL) = data;
}

void rg750_send_hint0()
{
  // Host Interrupt 0
  uint16_t data = MMIO16(RG750_HSTCTL);
  data &= 0xFFF0;
  data |= 0x0008;
  MMIO16(RG750_HSTCTL) = data;
}

void rg750_set_displaylist(uint16_t *dl)
{
  //printf("RG750: Setting AFG_ENTRY display list to %08X\n", dl);
  rg750_set_hstaddr((uint16_t *)RG750_HINT0_AFG_ENTRY);
  rg750_set_hstdata((uint16_t)((uint32_t)dl & 0xFFFF));
  rg750_set_hstdata((uint16_t)((uint32_t)dl >> 16));
}

uint8_t rg750_wait_for_interrupt()
{
  while((MMIO32(0xFFF40068) & 0x20) == 0) {}
  return rg750_get_interrupt_num();
}

void setup_vme_irq()
{
  //printf("Setting IRQ6 ISR\n");
  //MMIO32(0x78) = (uint32_t)irq6_isr;

  uint32_t ilr4 = MMIO32(0xFFF40084);
  ilr4 = 0x00600000;
  MMIO32(0xFFF40084) = ilr4;

  uint32_t lbie = MMIO32(0xFFF4006C);
  lbie |= 0x20;
  MMIO32(0xFFF4006C) = lbie;

  uint32_t iocr1 = MMIO32(0xFFF40088);
  iocr1 |= 0x00800000;
  MMIO32(0xFFF40088) = iocr1;
}

void rg750_transfer_68k_to_gsp(uint16_t *destination, uint16_t *source, uint32_t words_to_transfer)
{
  uint16_t *old = rg750_get_hstaddr();

  rg750_set_hstaddr(destination);
  for(int i=0; i<words_to_transfer; i++)
  {
    rg750_set_hstdata(*source++);
  }

  rg750_set_hstaddr(old);
}