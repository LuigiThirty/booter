CC = m68k-elf-gcc
AS = m68k-elf-as
LD = m68k-elf-ld
AR = m68k-elf-ar
OBJCOPY = m68k-elf-objcopy

SRCDIR = ./src
OBJDIR = ./obj
BINDIR = ./bin
DATADIR= ./data

CCFLAGS = -c -mcpu=68040 -I$(SRCDIR) -lm
LDFLAGS = -Tmvme162.ld -mcpu=68040 -lm

$(BINDIR)/hello.bin: $(BINDIR)/hello.elf
	m68k-elf-objcopy -O binary $< $@
	cp $@ /mnt/c/vme68k

$(OBJDIR)/bundle.o : $(DATADIR)/bundle.wad
	$(OBJCOPY) -I binary -O elf32-m68k $^ $@

$(OBJDIR)/aigis/aigis.a: $(OBJDIR)/aigis/aigis.o $(OBJDIR)/aigis/dlist.o $(OBJDIR)/aigis/rect.o $(OBJDIR)/aigis/xy.o $(OBJDIR)/aigis/xyz.o \
						 $(OBJDIR)/aigis/color.o $(OBJDIR)/aigis/fixedpt.o $(OBJDIR)/aigis/text.o $(OBJDIR)/aigis/pen.o $(OBJDIR)/aigis/line.o \
						 $(OBJDIR)/aigis/page.o $(OBJDIR)/aigis/bmp.o $(OBJDIR)/aigis/pixel.o $(OBJDIR)/aigis/sprite.o
	$(AR) cr $@ $^

$(OBJDIR)/afgis/afgis.a: $(OBJDIR)/afgis/afgis.o
	$(AR) cr $@ $^

$(OBJDIR)/mvme/mvme.a: $(OBJDIR)/mvme/syscall.o
	$(AR) cr $@ $^

$(OBJDIR)/raycast/raycast.a: $(OBJDIR)/raycast/r_raycst.o $(OBJDIR)/raycast/g_world.o $(OBJDIR)/raycast/w_wadmgr.o $(OBJDIR)/raycast/r_txdata.o \
							 $(OBJDIR)/raycast/r_wall.o $(OBJDIR)/raycast/g_frame.o $(OBJDIR)/raycast/i_inputs.o
	$(AR) cr $@ $^

$(BINDIR)/hello.elf: 	$(OBJDIR)/main.o $(OBJDIR)/raycast/raycast.a $(OBJDIR)/rg750.o $(OBJDIR)/math2d.o $(OBJDIR)/json.o $(OBJDIR)/aigis/aigis.a  \
						$(OBJDIR)/mvme/mvme.a $(OBJDIR)/afgis/afgis.a $(OBJDIR)/bundle.o \
					 	/home/luigi/m68k/gcc/lib/gcc/m68k-elf/10.1.0/../../../../m68k-elf/lib/m68040/libm.a
	$(CC) $(LDFLAGS) -o $@ $^
	
$(OBJDIR)/raycast/%.o : $(SRCDIR)/raycast/%.c
$(OBJDIR)/aigis/%.o : $(SRCDIR)/aigis/%.c
$(OBJDIR)/afgis/%.o : $(SRCDIR)/afgis/%.c
$(OBJDIR)/mvme/%.o : $(SRCDIR)/mvme/%.c
$(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(CC) $(CCFLAGS) -o $@ $<

$(OBJDIR)/mvme/%.o : $(SRCDIR)/mvme/%.asm
	$(AS) $(ASFLAGS) -o $@ $<

.PHONY: clean

clean:
	-rm ./bin/*
	-rm ./obj/*
	-rm ./obj/aigis/*
	-rm ./obj/afgis/*
	-rm ./obj/raycast/*